#ifndef WRITE_FUNCTIONS_H_INCLUDED
#define WRITE_FUNCTIONS_H_INCLUDED

#include "commonHeaders.h"

bool writeArrayToMatlabFile(const char* path, double* arr, int length);

bool write2DarrayToMatlabFile(const char* path, double** arr, int length1, int length2);

bool printArray(double* arr, int length);
bool printArray(int* arr, int length);
bool printArray(unsigned int* arr, int length);
bool print2DArray(double** arr, int len1, int len2);

#endif
