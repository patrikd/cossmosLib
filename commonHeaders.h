#ifndef COMMON_H
#define COMMON_H
#include "AnasaziBlockKrylovSchurSolMgr.hpp"
#include "AnasaziBasicEigenproblem.hpp"
#include "AnasaziConfigDefs.hpp"
#include "AnasaziEpetraAdapter.hpp"
#include "mpi.h"
#include "Epetra_SerialComm.h"
#include "Epetra_MpiComm.h"
#include "Ifpack.h"
#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Epetra_CrsMatrix.h"
#include "Epetra_LinearProblem.h"
#include "EpetraExt_MultiVectorOut.h"
#include "EpetraExt_MultiVectorIn.h"
#include "EpetraExt_RowMatrixOut.h"
#include "Amesos.h"
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <ctime>
#include <fstream>
#include <regex>
#include <dlfcn.h>
#include <chrono>
#include <thread>
//#include "Teuchos_StandardCatchMacros.hpp"
//#include "umfpack.h"
#include "AztecOO.h"
#include "AztecOO_Operator.h"
#include "AmesosGenOp.h"
//#include "MassActionKinetics.h"
#include <sbml/SBMLTypes.h>
#include "cossmosSetups.h"
//#include "ForBES.h"


using Teuchos::RCP;
using Teuchos::rcp;

typedef Epetra_MultiVector MV;
typedef Epetra_Vector V;
typedef Epetra_Operator OP;
typedef Anasazi::MultiVecTraits<double, MV> MVT;

//TODO: get rid of all Teuchos-Pointers (are not threadsafe)
typedef Teuchos::RCP<Epetra_CrsMatrix> TMatrix;
typedef Teuchos::RCP<V> TVector;
typedef Teuchos::RCP<MV> TMultiVector;
typedef Teuchos::RCP<Epetra_Map> TMap;

#endif
