#include "fGenerator.h"

namespace fGen
{
	enum class fType {LINEAR, SQUARE, CUBE,  HILL, HILL_SQUARE, HILL_CUBE, HILL_CROSS, HILL_LINEAR_SQUARE_CROSS,
						HILL_SQUARE_LINEAR_CROSS, CROSS, LINEAR_SQUARE_CROSS, SQUARE_LINEAR_CROSS, UNKNOWN};
}

fGen::fGenerator::fGenerator(std::string s, int numberOfSpecies)
:numSpecies(numberOfSpecies)
{
	//construct a linear f generator
	std::smatch m;
	std::regex lin_ex("^x\\d$", std::regex_constants::ECMAScript);
	if (std::regex_search(s, m, lin_ex))
	{
		type = fType::LINEAR;
		speciesIDs = new int[1];
		speciesIDs[0] = findID(s, false);
		return;
	}

	//construct a square f generator
	std::regex sqr_ex("^x\\d\\^2$");
	if (std::regex_search(s, m, sqr_ex))
	{
		type = fType::SQUARE;
		speciesIDs = new int[1];
		speciesIDs[0] = findID(s, false);
		return;
	}

	std::regex cube_ex("^x\\d\\^3$");
	if (std::regex_search(s, m, cube_ex))
	{
		type = fType::CUBE;
		speciesIDs = new int[1];
		speciesIDs[0] = findID(s, false);
		return;
	}

	//construct a hill f generator
	std::regex hill_ex("^1/\\(1\\+x\\d\\)$");
	if (std::regex_search(s, m, hill_ex))
	{
		type = fType::HILL;
		speciesIDs = new int[1];
		speciesIDs[0] = findID(s, false);
		return;
	}

	std::regex hill_sqr_ex("^1/\\(1\\+x\\d\\^2\\)$");
	if (std::regex_search(s, m, hill_sqr_ex))
	{
		type = fType::HILL_SQUARE;
		speciesIDs = new int[1];
		speciesIDs[0] = findID(s, false);
		return;
	}

	std::regex hill_cube_ex("^1/\\(1\\+x\\d\\^3\\)$");
	if (std::regex_search(s, m, hill_cube_ex))
	{
		type = fType::HILL_CUBE;
		speciesIDs = new int[1];
		speciesIDs[0] = findID(s, false);
		return;
	}

	//construct a cross f generator
	std::regex cross_ex("^x\\d\\*x\\d$");
	if (std::regex_search(s, m, cross_ex))
	{
		type = fType::CROSS;
		speciesIDs = new int[2];
		speciesIDs[0] = findID(s, false);
		speciesIDs[1] = findID(s, true);
		return;
	}

	std::regex hill_cross_ex("^1/\\(1\\+x\\d\\*x\\d\\)$");
	if (std::regex_search(s, m, hill_cross_ex))
	{
		type = fType::HILL_CROSS;
		speciesIDs = new int[2];
		speciesIDs[0] = findID(s, false);
		speciesIDs[1] = findID(s, true);
		return;
	}

	std::regex lin_sqr_cross_ex("^x\\d\\*x\\d\\^2$");
	if (std::regex_search(s, m, lin_sqr_cross_ex))
	{
		type = fType::LINEAR_SQUARE_CROSS;
		speciesIDs = new int[2];
		speciesIDs[0] = findID(s, false);
		speciesIDs[1] = findID(s, true);
		return;
	}

	std::regex hill_lin_sqr_cross_ex("^1/\\(1\\+x\\d\\*x\\d\\^2\\)$");
	if (std::regex_search(s, m, hill_lin_sqr_cross_ex))
	{
		type = fType::HILL_LINEAR_SQUARE_CROSS;
		speciesIDs = new int[2];
		speciesIDs[0] = findID(s, false);
		speciesIDs[1] = findID(s, true);
		return;
	}

	std::regex sqr_lin_cross_ex("^x\\d\\^2\\*x\\d$");
	if (std::regex_search(s, m, sqr_lin_cross_ex))
	{
		type = fType::SQUARE_LINEAR_CROSS;
		speciesIDs = new int[2];
		speciesIDs[0] = findID(s, false);
		speciesIDs[1] = findID(s, true);
		return;
	}

	std::regex hill_sqr_lin_cross_ex("^1/\\(1\\+x\\d\\^2\\*x\\d\\)$");
	if (std::regex_search(s, m, hill_sqr_lin_cross_ex))
	{
		type = fType::HILL_SQUARE_LINEAR_CROSS;
		speciesIDs = new int[2];
		speciesIDs[0] = findID(s, false);
		speciesIDs[1] = findID(s, true);
		return;
	}

	type = fType::UNKNOWN;
	fprintf(stderr, "%s\n", "ERROR: Constructed unknown typed fGenerator!");
	exit(EXIT_FAILURE);
}

fGen::fGenerator::~fGenerator(void)
{
	if (type != fType::UNKNOWN) delete[] speciesIDs;
}

double fGen::fGenerator::generate(std::string* strs)
{
	switch (type)
	{
	case fType::LINEAR:
		return std::stod(strs[speciesIDs[0]]);

	case fType::SQUARE:
		return std::pow(std::stod(strs[speciesIDs[0]]), 2.0);

	case fType::CUBE:
		return std::pow(std::stod(strs[speciesIDs[0]]), 3.0);

	case fType::HILL:
		return 1.0 / (1.0 + std::stod(strs[speciesIDs[0]]));

	case fType::HILL_SQUARE:
		return 1.0 / (1.0 + std::pow(std::stod(strs[speciesIDs[0]]), 2.0));

	case fType::HILL_CUBE:
		return 1.0 / (1.0 + std::pow(std::stod(strs[speciesIDs[0]]), 3.0));

	case fType::HILL_CROSS:
		return 1.0 / (1.0 + std::stod(strs[speciesIDs[0]]) * std::stod(strs[speciesIDs[1]]));

	case fType::HILL_LINEAR_SQUARE_CROSS:
		return 1.0 / (1.0 + std::stod(strs[speciesIDs[0]]) * std::pow(std::stod(strs[speciesIDs[1]]), 2.0));

	case fType::HILL_SQUARE_LINEAR_CROSS:
		return 1.0 / (1.0 + std::pow(std::stod(strs[speciesIDs[0]]), 2.0) * std::stod(strs[speciesIDs[1]]));

	case fType::CROSS:
		return std::stod(strs[speciesIDs[0]]) * std::stod(strs[speciesIDs[1]]);

	case fType::LINEAR_SQUARE_CROSS:
		return std::stod(strs[speciesIDs[0]]) * std::pow(std::stod(strs[speciesIDs[1]]), 2.0);

	case fType::SQUARE_LINEAR_CROSS:
		return std::pow(std::stod(strs[speciesIDs[0]]), 2.0) * std::stod(strs[speciesIDs[1]]);

	case fType::UNKNOWN:
		return nan("");

	default:
		fprintf(stderr, "%s\n", "ERROR: fGenerator could not generate requested type!");
		exit(EXIT_FAILURE);
	}
}

double fGen::fGenerator::generate(unsigned int* strs)
{
	switch (type)
	{
	case fType::LINEAR:
		return (double) strs[speciesIDs[0]];

	case fType::SQUARE:
		return std::pow((double) strs[speciesIDs[0]], 2.0);

	case fType::CUBE:
		return std::pow((double) strs[speciesIDs[0]], 3.0);

	case fType::HILL:
		return 1.0 / (1.0 + ((double) strs[speciesIDs[0]]));

	case fType::HILL_SQUARE:
		return 1.0 / (1.0 + std::pow((double) strs[speciesIDs[0]], 2.0));

	case fType::HILL_CUBE:
		return 1.0 / (1.0 + std::pow((double) strs[speciesIDs[0]], 3.0));

	case fType::HILL_CROSS:
		return 1.0 / (1.0 + ((double) strs[speciesIDs[0]]) * ((double) strs[speciesIDs[1]]));

	case fType::HILL_LINEAR_SQUARE_CROSS:
		return 1.0 / (1.0 + ((double) strs[speciesIDs[0]]) * std::pow(((double) strs[speciesIDs[1]]), 2.0));

	case fType::HILL_SQUARE_LINEAR_CROSS:
		return 1.0 / (1.0 + std::pow(((double) strs[speciesIDs[0]]), 2.0) * ((double) strs[speciesIDs[1]]));

	case fType::CROSS:
		return ((double) strs[speciesIDs[0]]) * ((double) strs[speciesIDs[1]]);

	case fType::LINEAR_SQUARE_CROSS:
		return ((double) strs[speciesIDs[0]]) * std::pow(((double) strs[speciesIDs[1]]), 2.0);

	case fType::SQUARE_LINEAR_CROSS:
		return std::pow(((double) strs[speciesIDs[0]]), 2.0) * ((double) strs[speciesIDs[1]]);

	case fType::UNKNOWN:
		return nan("");

	default:
		fprintf(stderr, "%s\n", "ERROR: fGenerator could not generate requested type!");
		exit(EXIT_FAILURE);
	}
}

int fGen::fGenerator::findID(std::string s, bool findSecond)
{
	std::size_t idx = s.find("x");
	if (findSecond) idx = s.find("x", idx+1);

	int id = s[idx+1] - '0' - 1; //impicit char to int conversion (char - '0')

	if (id < 0 || id >= numSpecies)
	{
		fprintf(stderr, "%s\n", "ERROR: fGenerator found an invalid species id");
		exit(EXIT_FAILURE);
	}

	return id;
}


fGen::Monomial::Monomial(int* exp, int numberOfSpecies)
:numSpecies(numberOfSpecies)
{
	exponent = new int[numSpecies];
	for (int i=0; i<numSpecies; i++)
	{
		exponent[i] = exp[i];
	}
}

fGen::Monomial::~Monomial(void)
{
	delete[] exponent;
}

double fGen::Monomial::compute(std::string* specCounts)
{
	double result = 1;
	for (int i=0; i<numSpecies; i++)
	{
		if (exponent[i] != 0)
		{
			result *= pow(std::stod(specCounts[i]), exponent[i]);
		}
	}

	return result;
}

double fGen::Monomial::compute(unsigned int* specCounts)
{
	double result = 1;
	for (int i=0; i<numSpecies; i++)
	{
		if (exponent[i] != 0)
		{
			result *= pow((double) specCounts[i], exponent[i]);
		}
	}

	return result;
}

//returns pointer to string object that has to be deleted!
std::string* fGen::Monomial::toString(void)
{
	bool firstPrinted = false;
	std::stringstream ss;

	for (int i=0; i<numSpecies; i++)
	{
		if (exponent[i] > 0)
		{
			if (firstPrinted)
				ss << "*";
			else
				firstPrinted = true;

			if (exponent[i] > 1)
				ss << "x" << i+1 << "^" << exponent[i];
			else
				ss << "x" << i+1;
		}
	}

	return new std::string(ss.str());
}
