#include "fillFunctions.h"


bool fillMatByArr(Epetra_CrsMatrix &A, double **a, int numRows)
{
	int* indicies = new int[numRows];
	for (int i=0; i<numRows; i++)
	{
		indicies[i] = i;
	}

	for (int row=0; row < numRows; row++)
	{

		if (A.MyGlobalRow(row))
		{
			A.InsertGlobalValues(row, numRows, a[row], indicies);
		}
	}

	A.FillComplete();

	delete[] indicies;
	return true;
}

bool fillMatByMultVec(Epetra_CrsMatrix &A, Epetra_MultiVector &mv, int numRows)
{
	for (int row=0; row < numRows; row++)
	{
		if (A.MyGlobalRow(row))
		{
			for (int col=0; col < numRows; col++)
			{
				double val = mv[col][row]; //this line is wrong for MPI
				A.InsertGlobalValues(row, 1, &val, &col);
			}
		}
	}

	A.FillComplete();

	return true;
}

bool fillMatDiagByVec(Epetra_CrsMatrix& A, Epetra_Vector* v, const Epetra_Map& Map, const Epetra_Comm& Comm)
{
	int numMyRows = v->MyLength();
	for (int myRow=0; myRow < numMyRows; myRow++)
	{
		//cout << Comm.MyPID() << ": " << myRow << endl;
		double vVal = (*v)[myRow];
		int gid = Map.GID(myRow);

		A.InsertGlobalValues(gid, 1, &vVal, &gid);
	}

	Comm.Barrier();
	A.FillComplete();

	return true;
}

/**
 * This is a method copied from
 * https://gist.github.com/jeetsukumaran/5392166 on 21.11.2017 at 11:07
 *
 * Calculates the binomial coefficient, $\choose{n, k}$, i.e., the number of
 * distinct sets of $k$ elements that can be sampled with replacement from a
 * population of $n$ elements.
 *
 * @tparam T
 *   Numeric type. Defaults to unsigned long.
 * @param n
 *   Population size.
 * @param k
 *   Number of elements to sample without replacement.
 *
 * @return
 *   The binomial coefficient, $\choose{n, k}$.
 *
 * @note
 *    Modified from: http://etceterology.com/fast-binomial-coefficients
 */
template <class T>
T binomial_coefficient(unsigned long n, unsigned long k) {
    unsigned long i;
    T b;
    if (0 == k || n == k) {
        return 1;
    }
    if (k > n) {
        return 0;
    }
    if (k > (n - k)) {
        k = n - k;
    }
    if (1 == k) {
        return n;
    }
    b = 1;
    for (i = 1; i <= k; ++i) {
        b *= (n - (k - i));
        if (b < 0) return -1; /* Overflow */
        b /= i;
    }
    return b;
}

void computeMaxNumMonomials(int* maxDegreeVector, int numSpecies, int& numMonomials)
{
	int maxDegree = 0;
	for (int i=0; i<numSpecies; i++)
	{
		if (maxDegree < maxDegreeVector[i]) maxDegree = maxDegreeVector[i];
	}

	numMonomials = 0;
	for (int d=1; d<=maxDegree; d++)
	{
		// use formula from https://en.wikipedia.org/wiki/Homogeneous_polynomial
		int add = binomial_coefficient<int>(d + numSpecies - 1, d);
		if (add != -1)
		{
			numMonomials += add;
		}
		else
		{
			fprintf(stderr, "%s\n", "overflow in binomial coefficient computation");
			exit(EXIT_FAILURE);
		}
	}
}

bool increaseExp(int* exp, int incIdx, int* maxDegVec, int numSpecies)
{

	if (exp[incIdx] == maxDegVec[incIdx])
	{
		if (incIdx != 0)
		{
			for (int i=incIdx; i<numSpecies; i++)
				exp[i] = 0;
			return increaseExp(exp, incIdx-1, maxDegVec, numSpecies);
		}
		else
		{
			return false;
		}
	}
	else
	{
		exp[incIdx] += 1;
		return true;
	}
}

int sumArr(int* exp, int numSpecies)
{
	int sum = 0;
	for (int i=0; i<numSpecies; i++)
	{
		sum += exp[i];
	}

	return sum;
}

bool isValidExpCombination(int* exp, int* maxDegreeVector, int numSpecies)
{
	for (int i=0; i<numSpecies; i++)
	{
		if (exp[i] > maxDegreeVector[i]) return false;
	}
	return true;
}

bool fillMonomialsArray(int* maxDegreeVector, fGen::Monomial** &monomial, int &numMonomials, int numSpecies)
{
	computeMaxNumMonomials(maxDegreeVector, numSpecies, numMonomials);
	monomial = new fGen::Monomial*[numMonomials];

	int exp[numSpecies];
	for (int i=0; i<numSpecies; i++)
		exp[i] = 0;
	exp[numSpecies-1] = 1;

	int numUsedMonomials = 0;
	do
	{
		if (isValidExpCombination(exp, maxDegreeVector, numSpecies)/*sumArr(exp, numSpecies) <= maxDegree*/)
		{
			monomial[numUsedMonomials] = new fGen::Monomial(exp, numSpecies);
			numUsedMonomials++;
		}
		//printArray(exp, numSpecies);
	}
	while (increaseExp(exp, numSpecies-1, maxDegreeVector, numSpecies));

	// at the start of this function "numMonomials" was set to the largest possible value
	// here we reset "numMonomials" to the number of monomials that was actually encountered
	numMonomials = numUsedMonomials;

	return true;
}
