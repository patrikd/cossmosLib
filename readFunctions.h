#ifndef READ_FUNCTIONS_H_INCLUDED
#define READ_FUNCTIONS_H_INCLUDED

#include "commonHeaders.h"
#include "fGenerator.h"
#include "fillFunctions.h"

int readMatrixRowsFromFile(const char* path, int &entriesPerRow);

//bool fillMatrixByFile(TMatrix &A, TMatrix &eye, const char* path);

bool readBFsFile(const char* desiredFpath, fGen::fGenerator** &fGens, int &numDesiredF, int numSpecies);

std::string readHeader(const char* headerFilePath);

std::vector<std::string> split(const std::string& s, char delimiter);

#endif
