#ifndef COSSMOS_H_INCLUDED
#define COSSMOS_H_INCLUDED

#include "cossmosResults.h"
#include "commonHeaders.h"
#include "readFunctions.h"
#include "fillFunctions.h"
#include "AugmentedModel.h"

/*!
 * \brief The main cossmosLib class providing the full cossmos() algorithm and the algorithms basisFunctionMethod() and computeStationaryDist()
 * that are also contained within the cossmos() algorithm.
 */
class cossmosAlgo
{
private:
	cossmosResults* res;
	bool hasResults;
	cossmosSetups* setups;

	bool printTimingInfo(clock_t begin, clock_t end, double &total_elapsed_secs, const std::string info, int PID);

	Epetra_Vector* getLScoefs(double **AArr, double* bArr, int n, Epetra_Map& smallMap,
								double& total_elapsed_secs, const Epetra_Comm& Comm, int maxIter, bool setVerboseSolvers);

	Epetra_Vector* getWeightedErrorCoefs(Epetra_Vector* f, Epetra_MultiVector& F, Epetra_Vector* pi, clock_t begin, int n, const Epetra_BlockMap& Map,
											Epetra_Map& smallMap, double& total_elapsed_secs, const Epetra_Comm& Comm);

	bool scaleWRTpi(Epetra_Vector* f, Epetra_Vector* pi, int NumMyElements, const Epetra_Comm& Comm, double &scaleFact);

	bool scaleWRTmaxAbs(Epetra_Vector* f, double &scaleFact);

	bool makeOrthogonalToPi(Epetra_Vector* f, Epetra_Vector* pi, Epetra_Vector* h, int NumMyElements);

	double dotWRTpi(Epetra_Vector &v1, Epetra_Vector &v2, TVector pi, Epetra_Comm &Comm);

	bool backTransform2(Epetra_Vector* c, double* scaleFact);

	void printApproxError(const Epetra_BlockMap& Map, Epetra_CrsMatrix* Qt, Epetra_Vector* g, Epetra_Vector* f, Epetra_Vector& pi, Epetra_Vector* h,
							int NumMyElements);

	double innerProduct(double* a, double* b, int length);

	void computeSensitivities(const Epetra_Comm& Comm, Epetra_Vector* g, AugmentedModel &augModel, double* fGathered, double* piGathered,
								std::ofstream& resultsFile, double* sensitivity);

public:
	/*! \brief The basis function method computes an approximate solution to the Poisson equation (Equation 4.16 in the thesis).
	 * It is explained in Section 4.7 of the thesis.
	 *
	 * \param desiredFvec A string specifying the function \f$ f \f$ used to obtain \f$ \hat{f}^* \f$ via Equation 4.15 in the thesis.
	 *
	 * \param stateEnum A pointer to an object that implements the virtual StateEnum class. It is required to vectorize all the functions used in the basis function
	 * method.
	 *
	 * \param Qt A pointer to the transpose of the scaled generator matrix \f$ Q_n^{*T} \f$. One can obtain that pointer by calling
	 * AugmentedModel::createQ().
	 *
	 * \param invPropSum Since basisFunctionMethod() uses the scaled generator matrix, this pointer to the scaling vector \a invPropSum (it is defined in Equation 4.5 of the
	 * thesis) is required during the basis function method to account for the scalings.
	 *
	 * \param pi A pointer to the vector holding the stationary distribution of CTMC model defined by the generator matrix.
	 * It can be computed with computeStationaryDist().
 	 *
	 * \returns The vector that approximates the solution vector to the Poisson equation (Equation 4.16 in the thesis).
	 */
	Epetra_Vector* basisFunctionMethod(const std::string& desiredFvec, StateEnum* stateEnum, Epetra_Vector* invPropSum, Epetra_Vector* pi,
											Epetra_CrsMatrix* Qt);

	/*! \brief This function computes the scaled and transformed stationary distribution \f$ \hat{\pi}^* \f$ with the sFSP algorithm (essentially finding
	 * a non trivial solution to Equation 4.9 in the thesis: \f$ Q_n^{*T} \hat{\pi}^* = 0 \f$).
	 *
	 * \param Qt A pointer to the transpose of the scaled generator matrix \f$ Q_n^{*T} \f$. One can obtain that pointer by calling
	 * AugmentedModel::createQ().
	 */
	// this method is based on an Trilinos example file
	// https://trilinos.org/docs/r12.12/packages/anasazi/doc/html/BlockKrylovSchur_2BlockKrylovSchurEpetraExGenAmesos_8cpp-example.html
	Epetra_Vector* computeStationaryDist(Epetra_CrsMatrix* Qt);

	/*! \brief This function executes the cossmos algorithm.
	 *
	 *  \param augModel An AugmentedModel object that describes the augmented model for the stochastic reaction network one wants to compute steady-state parametric sensitivities of.
	 *
	 *  \param Comm The \a Comm object handles the MPI communication between processors. \a Comm is either an \a Epetra_MpiComm object (for distributed computing) or an \a Epetra_SerialComm object (for serial computing).
	 *  Both are classes that are derived from the virtual class \a Epetra_Comm.
	 *
	 *  \return A pointer to a cossmosResults object containing the estimated steady-state parametric sensitivities. The cossmosResults object pointed to
	 *  is allocated on the heap during the call to cossmos() and is deleted as soon as the destructor
	 *  ~cossmosAlgo() is called. Thus the cossmos results have to be copied, should they be required beyond the lifetime of the respective cossmosAlgo
	 *  object. Also note that the sensitivity estimation results are only correct on the processor with the processor ID 0 (this is because the sensitivity
	 *  estimates are computed in a serial fashion on only one processor, namely processor 0, and they are
	 *  not distributed to the other processors afterwards). In case you are using multiple processors, make sure that only the processor with ID 0 is
	 *  processing the returned cossmosResults object (e.g. by using the if-clause "if(Comm.MyPID() == 0)").
	 * */
	cossmosResults* cossmos(AugmentedModel &augModel, Epetra_Comm &Comm);


	/*! \brief
	 *  \param setups With this pointer to a setup object the user can choose from different flavors of the cossmos algorithm. The cossmosAlgo object
	 *  stores only the pointer to the setup object and does not attain its own copy of the setup object! Thus changes in the setups object after the
	 *  cossmosAlgo construction will still be effective.
	 */
	cossmosAlgo(cossmosSetups* setups);

	~cossmosAlgo();

private:
	bool getHasResults() const;
};

#endif
