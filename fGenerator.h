#ifndef F_GENERATOR_H_INCLUDED
#define F_GENERATOR_H_INCLUDED

#include "commonHeaders.h"

namespace fGen
{
	enum class fType;

	/*!
	 * \brief This is class is not relevant for the cossmosLib user.
	 */
	class fGenerator
	{
	private:
		fType type;
		int* speciesIDs;
		int numSpecies;

	public:
		fGenerator(std::string s, int numberOfSpecies);
		~fGenerator(void);
		double generate(std::string* strs);
		double generate(unsigned int* strs);

	private:
		int findID(std::string s, bool findSecond);
	};

	/*!
	 * \brief This is class is not relevant for the cossmosLib user.
	 */
	class Monomial
	{
	private:
		int* exponent;
		int numSpecies;

	public:
		Monomial(int* exp, int numberOfSpecies);
		~Monomial(void);
		double compute(std::string* specCounts);
		double compute(unsigned int* specCounts);
		//returns pointer to string object that has to be deleted!
		std::string* toString(void);
	};
}

#endif
