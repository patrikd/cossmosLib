#include "cossmosResults.h"

cossmosResults::cossmosResults(unsigned int numExpFncts, unsigned int numSensFncts, unsigned int numParams)
{
	this->numExpFncts = numExpFncts;
	this->numSensFncts = numSensFncts;
	this->numParams = numParams;

	expecVals = new double[numExpFncts];

	sensVals = new double*[numSensFncts];
	for (unsigned int i=0; i<numSensFncts; i++)
	{
		sensVals[i] = new double[numParams];
	}
}

cossmosResults::~cossmosResults()
{
	delete[] expecVals;

	for (unsigned int i=0; i<numSensFncts; i++)
	{
		delete[] sensVals[i];
	}
	delete[] sensVals;
}

bool cossmosResults::print()
{
	std::cout << std::setprecision(4) << "\nThis cossmosResults object holds the following results:\n\n";
	std::cout << "expectations:\n";
	for (unsigned int i=0; i<numExpFncts; i++)
	{
		std::cout << expecVals[i];
		if (i+1 != numExpFncts) std::cout << ", ";
	}
	std::cout << std::endl;


	std::cout << "\nsensitivities:\n";
	for (unsigned int i=0; i<numSensFncts; i++)
	{
		std::cout << "function " << i << ":\n";
		for (unsigned int j=0; j<numParams; j++)
		{
			std::cout << sensVals[i][j];
			if (j+1 != numParams) std::cout << ", ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	return true;
}
