/*! \mainpage cossmosLib Documentation
 *
 * \section instal_sec Module versions
 *
 * We strongly recommend to use a Unix-like operating system to build and use \a cossmosLib. This is because we did not test anything for other operating systems. 
 *
 * Also it is strongly recommended to use a plattform that has Intel processors. This is because only then you can use the Intel MKL and Intel MPI distributions. 
 * For other plattfroms one needs different MPI, BLAS and LAPACK distributions which are usually slower than the ones from Intel.
 *
 * \a cossmosLib requires the use of third-party packages, libraries and programs (here referred to as "modules") in order to be built and executed. In the following figure one can see 
 * three example setups which we successfully used to built and execute programs that employ \a cossmosLib features. The left setup refers to a laptop setup, which we used to conduct local
 * test runs on a laptop. The middle setup refers to a cluster setup, which we used to conduct more challenging runs on a cluster computer.
 * The right setup refers to a second laptop which we used to conduct the steps given in the installation procedure tutorial.
 *
 * Note that METIS is part of the ParMETIS distribution and does not need to be installed separately.
 *
 * \image html moduleVersions.png
 *
 * \section tut Installation Procedure Tutorial
 * This installation procedure tutorial describes step-by-step how one can get a working \a cossmosLib distribution.
 * The tutorial assumes that one has only the core operating system programs installed. Everything else that is required to build \a cossmosLib
 * will be installed during the course of this tutorial. For the tutorial Ubuntu 18.04.1 was used. 
 * For other Unix-like opperating sytems the tutorial steps will slightly differ, but should in principle also be applicable. 
 *
 * The software version numbers stated in this tutorial are those that have been used while performing this example installation procedure. 
 * One does not necessarily need to use those software versions unless it is explicitly stated.
 *
 * Whenever paths are shown in this tutorial, it is very likely that you have to adjust those paths to your system.
 *
 * \subsection step0 Install the very basics
 * -# install g++ with:
 *
 *			sudo apt-get install g++
 *
 * -# install make with:
 *
 *			sudo apt-get install make
 *
 * -# install cmake with:
 *
 *			sudo apt-get install cmake
 *
 * -# install gfortran with:
 *
 *			sudo apt-get install gfortran
 *
 * -# install libxml2 with:
 *
 *			sudo apt-get install libxml2-dev
 *
 * \subsection step1 Install Intel MPI and Intel MKL
 *
 * -# Register for the Intel Performance Libraries (contains Intel MPI and Intel MKL) on the <a href="https://software.seek.intel.com/performance-libraries"> Intel website </a>
 *
 * -# Check your mails and click the download link that Intel sent you 
 *
 * -# Download the "Intel Math Kernel Library" and the "Intel MPI Library" (both 2018 Update 3)
 *
 * -# Unpack the downloaded source folders for Intel MKL and Intel MPI
 *
 * -# run the "install.sh" script in the unpacked Intel MPI folder as root and follow along:
 *
 *			sudo sh install.sh
 *
 * -# run the "install.sh" script in the unpacked Intel MKL folder as root and follow along:
 *
 *			sudo sh install.sh 
 *
 * \subsection step2 Install ParMETIS
 * <ol>
 * <li> Download the ParMETIS source code (4.0.3) from the <a href="http://glaros.dtc.umn.edu/gkhome/metis/parmetis/download"> Karypis lab website </a>
 *
 * <li> Unpack the downloaded source folder
 *
 * <li> In the ParMETIS directory go to "metis/include/metis.h" and change "#define IDXTYPEWIDTH 32" to "#define IDXTYPEWIDTH 64" and do the same for REALTYPEWIDTH below
 *
 * <li> In the ParMETIS directory run
 *
 *				 make config cc=/opt/intel/impi/2018.3.222/intel64/bin/mpicc cxx=/opt/intel/impi/2018.3.222/intel64/bin/mpicxx
 *
 * to configure make such that it uses the Intel MPI compilers and then run	
 *
 *		 make
 *
 * to build the ParMETIS library and finally 
 *
 *		 sudo make install
 *
 * to install it
 *
 * </ol>
 *
 * \subsection step3 Install SuperLU_dist
 * <ol>
 * <li> Download SuperLU_dist source code from the <a href="http://crd-legacy.lbl.gov/~xiaoye/SuperLU/#superlu_dist"> website of the Computational Reasearch Division of the Berkeley Lab Computing Sciences</a>.
 * We strongly advise you to use version 5.3.0 (or older), since we could not successfully build newer versions.
 *
 * <li> Unpack the downloaded source folder
 *
 * <li> In the SuperLU_dist directory run
 *
 *				 cp MAKE_INC/make.i386_linux make.inc
 *
 * to copy an example make include file into SuperLU_dist's main directory
 *
 * <li> open make.inc and change PARMETIS_DIR to the location of the ParMETIS directory (i.e. "${HOME}/parmetis-4.0.3"), 
 * 
 * add a # in front of 
 * BLASDEF to comment it out, 
 *
 * change BLASLIB to "-L/opt/intel/compilers_and_libraries_2018.3.222/linux/mkl/lib/intel64 -lmkl_intel_lp64 -lmkl_sequential -lmkl_core" to indicate where the BLAS library can be found (it is an integral part of Intel MKL), 
 * 
 * change both FORTRAN and LOADER to "/opt/intel/impi/2018.3.222/intel64/bin/mpif90" to indicate where the MPI Fortran-compiler can be found, 
 *
 * change CC to "/opt/intel/impi/2018.3.222/intel64/bin/mpicc" to indicate where the MPI C-compiler can be found 
 *
 * and finally change DSuperLUroot to the SuperLU_dist directory (i.e. "${HOME}/superlu_dist_5.3.0/SuperLU_DIST_5.3.0")
 *
 * <li> Create a folder named "lib" in the SuperLU_dist directory
 *
 * <li> run 
 *
 *				 	 make
 *
 * in the SuperLU_dist directory to build the SuperLU_dist library
 *
 * </ol>
 *
 *
 * \subsection step4 Install Trilinos
 * <ol>
 * <li> Download the Trilinos source code (12.12.1) from the <a href="https://trilinos.org/download/"> Trilinos website </a>. 
 * For that one needs to go through a registration process, should one not be a registrated user already.
 *
 * <li> Unpack the downloaded source folder
 *
 * <li> Create a TriliniosBuild directory (e.g. in your home directory)
 *
 * <li> In the TrilinosBuild directory create a new file called do-configure.sh and fill it with the content shown in the following:
 * \include docFiles/do-configure.sh
 * With this configuration script one configures what Trillinos packages should be installed, what compilers should be used during the building of Trilinos and 
 * where all the include- and library-files of the third party libraries can be found. Finally in the two last lines the locations of the 
 * TrilinosBuild directory and the Trilinos source directory are specified.
 *
 * <li> In the TrilinosBuild directory run
 *
 *				 sh do-configure.sh
 *
 * to configure the build process
 *
 * <li> run 
 *
 *					 sudo cp $HOME/parmetis-4.0.3/metis/include/metis.h /usr/local/include/metis.h
 *
 * to prevent an error during the Trilinos build process caused by an unfound metis.h header file. 
 * Note that in the upper command "$HOME" is a placeholder for the path to your home directory and needs to be replaced.
 *
 * <li> In the TrillinosBuild directory run
 *
 *				 make
 *
 * and
 *
 *		 sudo make install
 *
 * to build and install Trilinos
 *
 * </ol>
 *
 * \subsection step5 Install libSBML
 * <ol>
 * <li> Download the libSBML source code (5.17.0-core-src) from the <a href="http://sbml.org/Software/libSBML/Downloading_libSBML/"> Systems Biology Markup Language website </a>.
 *
 * <li> Unpack the downloaded source folder
 *
 * <li> In the libSBML directory run
 *
 *				 ./congfigure
 *
 * followed by
 *
 *		 make
 *
 * and
 *
 *		 sudo make install 
 *
 * </ol>
 *
 * \subsection step6 Install cossmosLib
 * <ol>
 * <li> Get the \a cossmosLib source folder
 *
 * <li> In the \a cossmosLib source directory open the file called makefile and:
 *
 * Change IMPICXX to the path of your MPI C++ compiler (e.g. /opt/intel/impi/2018.3.222/intel64/bin/mpicxx in this example) and set both TRILINOS_INCLUDE and FURTHER_INCLUDES to the empty 
 * string (i.e. leave them blank). We can leave the TRILINOS_INCLUDE and FURTHER_INCLUDES paths empty, because of the "sudo make install" steps that were performed during the Trilinos and 
 * libSBML installation.
 * These steps copy the respective include files to a location that is known to the operating system. Thus the OS will automatically find these include files.
 *
 * <li> In the \a cossmosLib source directory run
 *
 *				 make
 *
 * to build the shared \a cossmosLib library file
 *
 * </ol>
 *
 * \section buildExec Build and execute a program that uses cossmosLib features
 * <ol>
 * <li> Write C++ code that uses \a cossmosLib (e.g. the example below, let us call it UseLib.cpp)
 *
 * <li> Put the following CMakeLists.txt into the same directory as UseLib.cpp:
 * \include docFiles/CMakeLists.txt
 *
 * <li> In the UseLib directory run
 *
 *				 cmake .
 *
 * and
 *
 *		 make
 *
 * to build an executable version of UseLib
 *
 * <li> In the UseLib directory run
 *
 *				 ./UseLib 
 *
 * to start the program
 *
 * </ol>
 *
 * \section example_sec cossmosLib example: UseLib.cpp
 *
 * \include docFiles/Example.cpp
 *
 * If you run the above program (on a single processor) using the following SBML and derivative files, the output should be something like:
 *
 * \include docFiles/output.txt
 *
 * Note that this example requires about 10GB of working memory. So if that is not provided, this example might crash due to lack of memory.
 * The last portion of the output (starting from "This cossmosResults object holds the following results") is the output produced by the call
 * to "if (comm.MyPID() == 0) res->print();"
 *
 * \subsection file1 SBML file: genexClean.sbml
 *
 * \include docFiles/genexClean.sbml
 *
 * The syntax of SBML files compatible with \a cossmosLib is described in Section 4.2 of the thesis.
 *
 * \subsection file3 partial derivatives file: derivativeFunctions.txt
 *
 * \include docFiles/derivativeFunctions.txt
 *
 * The syntax of partial derivative files compatible with \a cossmosLib is described in Section 4.5 of the thesis.
 *
 * \section thesis_sec Thesis
 *
 * The \a cossmosLib has been developed as part of a Master's thesis project at ETH Zurich.
 * The report of the thesis contains a lot of valuable information that explains what \a cossmosLib does and how it can be used. Often some section
 * or equation of the thesis is referenced in this HTML-documentation. Thus make sure you get a copy of the thesis, if you want to use the
 * \a cossmosLib library.
 *
 * The title of the thesis is "Development of computational methods for efficient estimation of steady-state parametric sensitivities for
 * stochastic models of reaction networks" and it can be downloaded from <a href="https://git.bsse.ethz.ch/patrikd/cossmosLib/uploads/84d9ab0d2cbc1f24d07ebdbb574c58a9/report_thesis_Patrik_Durrenberger.pdf"> our git repository </a>
 *
 * Note that at the time the thesis was written, \a cossmosLib was not a library but a regular program. Thus the thesis contains a section on how
 * to call that program that explains what program parameters there are and how to correctly specify them (Section 4.11). That section is no longer
 * interesting to \a cossmosLib users, because the cossmos algorithm parameters are now defined as stated in this documentation. There is also a
 * subsection on how to install the cossmos program (Subsection 4.10.1) that is no longer useful for \a cossmosLib users. \a cossmosLib users should
 * refer to the Installation section in this documentation instead. Similarly Subsection 4.10.2 explains how to install Trilinos. This can now also be learned in this documentation instead.
 *
 *
 */
