
# modify these paths according to your platfrom

# path to the MPI C++ complier
IMPICXX=/opt/intel/impi/2017.4.239/intel64/bin/mpicxx

# path to the include directory of your Trilinos installation
TRILINOS_INCLUDE=$(HOME)/TrilinosWithIntel/include

# path to further include directories (e.g. directory containing libSBML headers)
FURTHER_INCLUDES=






# do not modify from here on
SRCS=AmesosGenOp.cpp AugmentedModel.cpp cossmos.cpp cossmosResults.cpp cossmosSetups.cpp fGenerator.cpp fillFunctions.cpp readFunctions.cpp StateEnum.cpp writeFunctions.cpp
OBJS=$(subst .cpp,.o,$(SRCS))


all: libcossmosLib.so libcossmosLib.a

libcossmosLib.a: $(OBJS)
	ar rvs libcossmosLib.a $(OBJS)

libcossmosLib.so: $(OBJS)
	$(IMPICXX) -shared -o "libcossmosLib.so" $(OBJS)

AmesosGenOp.o: AmesosGenOp.h AmesosGenOp.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"AmesosGenOp.d" -MT"AmesosGenOp.d" -o "AmesosGenOp.o" "AmesosGenOp.cpp"

AugmentedModel.o: AugmentedModel.h AugmentedModel.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"AugmentedModel.d" -MT"AugmentedModel.d" -o "AugmentedModel.o" "AugmentedModel.cpp"

cossmos.o: cossmos.h cossmos.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"cossmos.d" -MT"cossmos.d" -o "cossmos.o" "cossmos.cpp"

cossmosResults.o: cossmosResults.h cossmosResults.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"cossmosResults.d" -MT"cossmosResults.d" -o "cossmosResults.o" "cossmosResults.cpp"

cossmosSetups.o: cossmosSetups.h cossmosSetups.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"cossmosSetups.d" -MT"cossmosSetups.d" -o "cossmosSetups.o" "cossmosSetups.cpp"

fGenerator.o: fGenerator.h fGenerator.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"fGenerator.d" -MT"fGenerator.d" -o "fGenerator.o" "fGenerator.cpp"

fillFunctions.o: fillFunctions.h fillFunctions.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"fillFunctions.d" -MT"fillFunctions.d" -o "fillFunctions.o" "fillFunctions.cpp"

readFunctions.o: readFunctions.h readFunctions.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"readFunctions.d" -MT"readFunctions.d" -o "readFunctions.o" "readFunctions.cpp"

StateEnum.o: StateEnum.h StateEnum.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"StateEnum.d" -MT"StateEnum.d" -o "StateEnum.o" "StateEnum.cpp"

writeFunctions.o: writeFunctions.h writeFunctions.cpp
	$(IMPICXX) -I$(TRILINOS_INCLUDE) -I$(FURTHER_INCLUDES) -O0 -g3 -Wall -c -fmessage-length=0 -std=c++0x -fPIC -MMD -MP -MF"writeFunctions.d" -MT"writeFunctions.d" -o "writeFunctions.o" "writeFunctions.cpp"


clean: 
	rm *.d
	rm *.o
	rm *.so
	rm *.a
