#ifndef COSSMOS_RESULTS_H_INCLUDED
#define COSSMOS_RESULTS_H_INCLUDED

#include "commonHeaders.h"

/*!
 * \brief An object that holds the results of a cossmos algorithm run.
 *
 * \note A cossmosResults object is automatically allocated on the heap during the call to cossmosAlgo::cossmos() and is deleted as soon as the
 * destructor cossmosAlgo::~cossmosAlgo() (of the object on which cossmosAlgo::cossmos() was called) is called. Thus the cossmos results have to be copied, should they be
 * required beyond the lifetime of the respective cossmosAlgo object. Also note that the sensitivity estimation results are only correct on the
 * processor with the processor ID 0 (this is because the sensitivity estimates are computed in a serial fashion on only one processor, namely
 * processor 0, and they are not distributed to the other processors afterwards). In case you are using multiple processors, make sure that only
 * the processor with ID 0 is processing the returned cossmosResults object (e.g. by using the if-clause "if(Comm.MyPID() == 0)").
 */
class cossmosResults
{
	double* expecVals;
	double** sensVals;

	unsigned int numExpFncts, numSensFncts, numParams;

public:
	/*! \brief Constructor of the cossmos results object. It is automatically used during calls to cossmosAlgo::cossmos() to construct the cossmosResults object which is returned by cossmosAlgo::cossmos().
	 *
	 * \param numExpFncts The number of functions \f$f_i\f$ for which \f$E[f_i(X)]\f$ is being estimated during a call to cossmosAlgo::cossmos(). The functions \f$f_i\f$ are specified by the parameter \a expectFncts in
	 * cossmosSetups::cossmosSetups().
	 *
	 * \param numSensFncts The number of functions \f$f_i\f$ for which the sensitivities \f$S_{\theta}(f_i,\{1, \dots, numParams\})\f$ are being estimated
	 * during calls to cossmosAlgo::cossmos(). The functions \f$f_i\f$ are specified by the parameter \a desiredFfncts in cossmosSetups::cossmosSetups().
	 *
	 * \param numParams The number of parameters \f$\theta_i\f$ of the reaction network model.
	 */
	cossmosResults(unsigned int numExpFncts, unsigned int numSensFncts, unsigned int numParams);
	~cossmosResults();

	/*!
	 * \brief This function prints all the estimated sensitivity and expectation values that this result object holds.
	 */
	bool print();

	/*!
	 * \returns The pointer to the first element of the array of estimated expectation values. Use getNumExpFncts() to receive the length of that array.
	 */
	double* getExpecVals() const {
		return expecVals;
	}

	/*!
	 * \returns The pointer to the first element of the two-dimensional array of estimated sensitivity values. Access the sensitivity estimate
	 * \f$S_{\theta}(f_i,j)\f$ with \a "getSensVals()[i][j]". Use getNumSensFncts() to receive the length of the first array dimension and
	 * getNumParams() to receive the length of the second array dimension.
	 */
	double** getSensVals() const {
		return sensVals;
	}

	/*!
	 * \returns The number of expectation functions.
	 */
	unsigned int getNumExpFncts() const {
		return numExpFncts;
	}

	/*!
	 * \returns The number of parameters of the reaction network model.
	 */
	unsigned int getNumParams() const {
		return numParams;
	}

	/*!
	 * \returns The number of sensitivity functions.
	 */
	unsigned int getNumSensFncts() const {
		return numSensFncts;
	}
};

#endif
