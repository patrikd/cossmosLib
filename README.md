# Installation

visit the [cossmosLib installation procedure page](https://git.bsse.ethz.ch/patrikd/cossmosLib/wikis/cossmosLib-Installation-Procedure)

# Documentation

after cloning the repository, run

<pre class="fragment"> doxygen doxgenConfig.txt</pre>

on the command line to create the HTML documentation for the _cossmosLib_

# Contact

For feedback or questions feel free to contact [Patrik Dürrenberger](mailto:patrik.duerrenberger@bsse.ethz.ch)
