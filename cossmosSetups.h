/*
 * CossmosParameters.h
 *
 *  Created on: May 24, 2018
 *      Author: patrik
 */

#ifndef COSSMOSPARAMETERS_H_
#define COSSMOSPARAMETERS_H_

#include "commonHeaders.h"

/*!
 * \brief An object that holds all the cossmos algorithm setups.
 *
 *  Unless the user specifies a setup variable with the respective setter function, reasonable default values are used. Thus the user does not
 *  have to tinker around with setup options he is not interested in.
 */

class cossmosSetups {
public:
	/*! \brief Constructor of the cossmos setups object.
	 *
	 *  \param expectFncts Specifies a set of functions \f$\{f_1 , . . . , f_q\}\f$ as a comma-separated string of function strings. The same rules as
	 *  for the elements of \a desiredFfncts apply to the function strings in \a expectFncts as well. For each function \f$f_i\f$ within the specified set the
	 *  expectation \f$E(f_i(X))\f$ over the stationary distribution \f$\pi\f$, estimated by the sFSP algorithm, is computed. Thereby the multivariate
	 *  random variable \f$X = [X_1 , \dots , X_M ]^T\f$ is the vector of random variables, where each \f$X_s\f$ represents the random count of species
	 *  \f$s\f$ in the steady-state. E.g. the string \a x1,x2 specifies the functions \f$ f_1(X) = X_1 \f$ and \f$ f_2(X) =X_2\f$
	 *
	 *  \param desiredFfncts Specifies a set of functions \f$\{f_1, \dots, f_q\}\f$ as a comma-separated string of function strings. For each function in
	 *  this set the program will output the steady-state parametric sensitivity with respect to each parameter. Thus, the program will output the
	 *  following set of sensitivity values: \f$\{ S_{{\theta}}(f_1, 1), \dots, S_{{\theta}}(f_1, P), S_{{\theta}}(f_2, 1), \dots, S_{{\theta}}(f_2, P),
	 *  \dots, S_{{\theta}}(f_q, 1), \dots, S_{{\theta}}(f_q, P)\}\f$. The functions \f$f_i\f$ one can use, are currently restricted to one of
	 *  the following: \f$ x_i, x^2_i, x^3_i , \frac{1}{1+x_i}, \frac{1}{1+x^2_i}, \frac{1}{1+x^3_i}, x_i x_j , \frac{1}{1+x_i x_j}, x_i x^2_j,
	 *  \frac{1}{1+x_i x^2_j}, x^2_i x_j, \frac{1}{1+x^2_i x_j} \f$ where \f$i, j \in \{1, \dots , s\} \f$ are species indicies. The strings that
	 *  represent these functions are the intuitive ones and it is renounced to list them all here.
	 *  Do not use space characters within the string of comma-separated function strings!
	 *  Of course \a desiredFfncts can also contain just one single function. In that case no comma is required. Some examples of
	 *  \a desiredFfncts strings and their respective  set of functions are: \a x1 \f$\rightarrow\f$ \f$\{x_1\}\f$; \a x1,x2 \f$\rightarrow\f$
	 *  \f$\{x_1, x_2\}\f$; \a x1,x2,x1^2,x2^2 \f$\rightarrow\f$ \f$\{x_1, x_2, x_1^2, x_2^2\}\f$; \a 1/(1+x5^2) \f$\rightarrow\f$ \f$\{\frac{1}{1+x_5^2}\}\f$
	 *
	 * \param numSpecies The number of species of the model that is going to be addressed with this cossmosSetup object.
	 */
	cossmosSetups(const char* expectFncts, const char* desiredFfncts, unsigned int numSpecies);
	virtual ~cossmosSetups();

	int getBlockSize() const {
		return blockSize;
	}

	/*! \brief
	 * \param blockSize Is a positive integer specifying the block Kyrlov-Schur solver’s block size.
	 * When the setup object is constructed the block size is set to a default value of 1.
	 * The block Kyrlov-Schur solver is used in cossmosAlgo::computeStationaryDist to compute the stationary distribution.
	 */
	void setBlockSize(int blockSize) {
		this->blockSize = blockSize;
	}

	double getConvergeTol() const {
		return convergeTol;
	}

	/*! \brief
	 * \param convergeTol Is a positive double value specifying how small the residual must be for the block Kyrlov-Schur solver to stop its iterative procedure and
	 * indicate convergence. When the setup object is constructed the convergence tolerance is set to a default value of 0.00000001.
	 * The block Kyrlov-Schur solver is used in cossmosAlgo::computeStationaryDist to compute the stationary distribution.
	 */
	void setConvergeTol(double convergeTol) {
		this->convergeTol = convergeTol;
	}

	int getNumBlocks() const {
		return numBlocks;
	}

	/*! \brief
	 * \param numBlocks Is a positive integer specifying the number of blocks allocated for the Krylov basis of the block Kyrlov-Schur solver.
	 * When the setup object is constructed the number of blocks is set to a default value of 3.
	 * The block Kyrlov-Schur solver is used in cossmosAlgo::computeStationaryDist to compute the stationary distribution.
	 */
	void setNumBlocks(int numBlocks) {
		this->numBlocks = numBlocks;
	}

	int getNumRestarts() const {
		return numRestarts;
	}

	/*! \brief
	 * \param numRestarts Is a positive integer specifying the maximum number of restarts the block Kyrlov-Schur solver is alowed to perform.
	 * When the setup object is constructed the number of restarts is set to a default value of 200.
	 * The block Kyrlov-Schur solver is used in cossmosAlgo::computeStationaryDist to compute the stationary distribution.
	 */
	void setNumRestarts(int numRestarts) {
		this->numRestarts = numRestarts;
	}

	bool isSetVerboseSolvers() const {
		return setVerboseSolvers;
	}

	/*! \brief
	 * \param setVerboseSolvers The cossmos program uses two iterative solvers: the Anasazi eigenvalue problem solver to compute the stationary distribution
	 *  in cossmosAlgo::computeStationaryDist and the AztecOO solver to compute the weights \f$c^*\f$ in the basis function method (see Equation 4.29
	 *  in the thesis). If set to \a true, the two iterative solvers print out additional information to the standard output. This information can help to
	 *  adjust some solver specific program parameters (like numBlocks, numRestarts, etc.). If set to false, no additional information is printed.
	 *  When the setup object is constructed \a setVerboseSolvers is set to false.
	 */
	void setSetVerboseSolvers(bool setVerboseSolvers) {
		this->setVerboseSolvers = setVerboseSolvers;
	}

	bool isStorePi() const {
		return storePi;
	}

	/*! \brief
	 * \param storePi If set to true, the unscaled, transformed stationary distribution vector \f$\pi^*\f$ is stored, right after it has been computed,
	 * in the file \a pi_[NetName].txt located in the same directory the program was called in. Thereby the placeholder [NetName] is replaced by the
	 * NetName string specified in this setup object. If set to false, \f$\pi^*\f$ is not stored in a file and lost after the program terminates.
	 * When the setup object is constructed \a storePi is set to false.
	 */
	void setStorePi(bool storePi) {
		this->storePi = storePi;
	}

	bool isComputePi() const {
		return computePi;
	}

	/*! \brief
	 * \param computePi If set to true, the program will actually compute the unscaled, transformed stationary distribution vector \f$\pi^*\f$.
	 * If set to false, the program tries to load \f$\pi^*\f$ from a file called \a pi_[networkName].txt located in the same directory the program
	 * was called in. Thereby the placeholder [NetName] is replaced by the NetName string specified in this setup object. Note that the file
	 * \a pi_[networkName].txt must have been created in an earlier run of the program (compare to the description of setStorePi(bool storePi)).
	 * If the program parameters have changed between the storing and loading of \a pi_[networkName].txt, the program might exit with an error or
	 * (in the worse case) finish regularly while returning wrong sensitivity values. Therefore one has to make absolutely sure to use identical
	 * program parameters in both the storage and loading run. In cases of doubt one should rather recompute \f$\pi^*\f$ by setting \a computePi to true,
	 * than risking to receive wrong sensitivity values.
	 * When the setup object is constructed \a computePi is set to true.
	 */
	void setComputePi(bool computePi) {
		this->computePi = computePi;
	}

	const std::string& getNetName() const {
		return netName;
	}

	/*! \brief
	 * \param netName An arbitrary string specifying the name of the reaction network. It is mainly used to store the transformed
	 * stationary distribution \f$\pi^*\f$ in a file with a reaction network specific name (refer to setComputePi(bool computePi) and
	 * setStorePi(bool storePi) for more details).
	 * When the setup object is constructed \a netName is set to the default string "myNet".
	 */
	void setNetName(const std::string& netName) {
		this->netName = netName;
	}

	const char* getDesiredFfncts() const {
		return desiredFfncts;
	}

	const char* getExpectFncts() const {
		return expectFncts;
	}

	const char* getResultsFilePath() const {
		return resultsFilePath;
	}

	int* getMaxDegree() const {
		return maxDegreeVector;
	}

	/*! \brief
	 * \param maxDegree In the mode where useBFsFile is set to false (see setUseBFsFile(bool useBFsFile)), \a maxDegreeVec is a vector of positive integers
	 * specifying the maximum degrees \f$d_{max}\f$ of monomials for all the species. These degrees are used to construct the set of basis functions
	 * \f$\{g_1 , \dots , g_m\}\f$ for the basis function method (compare to Section 4.7 of the thesis). \a maxDegreeVec is ignored (i.e. it can be set to an
	 * arbitrary vector or a null pointer) if useBFsFile is set to true.
	 * When the setup object is constructed \a maxDegreeVec is set to a default vector containing all 1's.
	 */
	void setMaxDegree(int* maxDegreeVec, unsigned int numSpecies)
	{
		for (unsigned int i=0; i<numSpecies; i++)
		{
			this->maxDegreeVector[i] = maxDegreeVec[i];
		}
	}

	int getMaxIterLs() const {
		return maxIterLS;
	}

	/*! \brief
	 * \param maxIterLs A positive integer specifying the maximum number of iterations the iterative AztecOO solver will perform when solving a linear system
	 * (see Equation 4.29 in the thesis) during the BFM. Note that \a maxIterLS is quite an unimportant parameter, because said system is usually small
	 * and easy to solve (i.e. requires only a small number of iterations). But in general it is still good to be able to control that parameter, which
	 * is why we chose not to hard-code it and provide it as a setup parameter.
	 * When the setup object is constructed \a maxIterLs is set to a default value of 10000.
	 */
	void setMaxIterLs(int maxIterLs) {
		maxIterLS = maxIterLs;
	}

	char* getBFsFilePath() const {
		return BFsFilePath;
	}

	/*! \brief
	 * \param BFsFilePath A string specifying the absolute or relative path to a file describing the set of basis functions \f$\{g_1 , \dots , g_m\}\f$
	 * used for the BFM if \a useBFsFile is set to true. The structure and syntax of this file was described in Section 4.7.1 of the thesis. If
	 * \a useBFsFile is set to false,  the value of \a BFsFilePath is ignored.
	 * When the setup object is constructed \a BFsFilePath is set to the empty string "".
	 */
	void setBFsFilePath(char* BFsFilePath) {
		this->BFsFilePath = BFsFilePath;
	}

	bool isUseBFsFile() const {
		return useBFsFile;
	}

	/*! \brief
	 * \param useBFsFile If set to true, the program uses the first mode (of the two modes described in Section 4.7.1 of the thesis) to define the set of basis
	 * functions \f$\{g_1 , \dots , g_m\}\f$ for the BFM. If set to false, the program uses the second mode. In the first mode the program also considers
	 * the setup parameter \a BFsFilePath, in the second mode it also considers \a maxDegree, to fully define the set of basis functions.
	 * When the setup object is constructed \a useBFsFile is set to false.
	 */
	void setUseBFsFile(bool useBFsFile) {
		this->useBFsFile = useBFsFile;
	}

	/*! \brief
	 * \param resultsFilePath A string specifying the absolute or relative path to a file where the cossmos results are stored if
	 * \a useResultsFile is set to true. If \a useResultsFile is set to false, the value of \a resultsFilePath is ignored.
	 * When the setup object is constructed \a resultsFilePath is set to the empty string "".
	 */
	void setResultsFilePath(const char* resultsFilePath) {
		this->resultsFilePath = resultsFilePath;
	}

	bool isUseResultsFile() const {
		return useResultsFile;
	}
	/*! \brief
	 * \param useResultsFile If set to true, the program writes the cossmos results in the file specified by \a resultsFilePath. If set to false, the
	 * program does not write the cossmos results to a file.
	 * When the setup object is constructed \a useResultsFile is set to false.
	 */
	void setUseResultsFile(bool useResultsFile) {
		this->useResultsFile = useResultsFile;
	}


	bool isMuteOutput() const {
			return muteOutput;
		}

	/*! \brief
	 * \param muteOutput If set to true, the program doesn't print any information to the standard output. If set to false, the
	 * program prints information to the standard output.
	 * When the setup object is constructed \a muteOutput is set to false.
	 */
	void setMuteOutput(bool muteOutput) {
		this->muteOutput = muteOutput;
	}

	bool isComputeOutflowRate() const
	{
		return computeOutflowRate;
	}

	/*! \brief
	 * \param computeOutflowRate If set to true, the program computes and prints the outflow rate (defined in Equation 3.2 of "A. Gupta, J. Mikelson, and M. Khammash.
	 * A finite state projection algorithm for the stationary solution of the chemical master equation. The Journal of Chemical Physics, 147(15):154101, 2017.") of
	 * the stationary distribution computed during the cossmos algorithm. If set to false, the program does not compute the outflow rate.
	 * When the setup object is constructed \a computeOutflowRate is set to true.
	 */
	void setComputeOutflowRate(bool computeOutflowRate)
	{
		this->computeOutflowRate = computeOutflowRate;
	}

//	bool setPrintStateEnum(bool pse);
//	bool getPrintStateEnum();
//
//	bool setSleepTime(int st);
//	int getSleepTime();

	//bool setResetParams(double* resetParams, int numParams);
	//double* getResetParams();
	//int getNumSpecies();

private:
	std::string netName;
	bool computePi, storePi, setVerboseSolvers, useBFsFile, useResultsFile, muteOutput, computeOutflowRate;
	int numBlocks, numRestarts, blockSize, maxIterLS;
	unsigned int numSpecies;
	int* maxDegreeVector;
	double convergeTol;
	const char* expectFncts, * desiredFfncts, * resultsFilePath;
	char* BFsFilePath;
//	bool printStateEnum;
//	int sleepTime;
	//bool doResetParams;
	//double* resetParams;
	//int numSpecies;
};

#endif /* COSSMOSPARAMETERS_H_ */
