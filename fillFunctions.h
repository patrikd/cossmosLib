#ifndef FILL_FUNCTIONS_H_INCLUDED
#define FILL_FUNCTIONS_H_INCLUDED

#include "commonHeaders.h"
#include "writeFunctions.h"
#include "fGenerator.h"

bool fillMatByArr(Epetra_CrsMatrix &A, double **a, int numRows);

bool fillMatByMultVec(Epetra_CrsMatrix &A, Epetra_MultiVector &mv, int numRows);

bool fillMatDiagByVec(Epetra_CrsMatrix& A, Epetra_Vector* v, const Epetra_Map& Map, const Epetra_Comm& Comm);

/**
 * This is a method copied from
 * https://gist.github.com/jeetsukumaran/5392166 on 21.11.2017 at 11:07
 *
 * Calculates the binomial coefficient, $\choose{n, k}$, i.e., the number of
 * distinct sets of $k$ elements that can be sampled with replacement from a
 * population of $n$ elements.
 *
 * @tparam T
 *   Numeric type. Defaults to unsigned long.
 * @param n
 *   Population size.
 * @param k
 *   Number of elements to sample without replacement.
 *
 * @return
 *   The binomial coefficient, $\choose{n, k}$.
 *
 * @note
 *    Modified from: http://etceterology.com/fast-binomial-coefficients
 */
template <class T = unsigned long>
T binomial_coefficient(unsigned long n, unsigned long k);

void computeNumMonomials(int maxDegree, int numSpecies, int& numMonomials);

bool increaseExp(int* exp, int incIdx, int maxDeg, int numSpecies);

int sumArr(int* exp, int numSpecies);

bool fillMonomialsArray(int* maxDegreeVector, fGen::Monomial** &monomial, int &numMonomials, int numSpecies);

#endif
