#include "AugmentedModel.h"


//TODO: what if SBML model has reversible (or fast) reactions, will my program still work?
AugmentedModel::AugmentedModel(const char* n, double* resetParamsArray, unsigned int dI, StateEnum* sE, const char* sbmlDocPath,
								const char* derivFilePath, const char* headerPath, Epetra_Comm &Comm, bool muteOutput)
								:name(n), stateEnum(sE), designatedIndex(dI), isMuteOutput(muteOutput)
{
	allocatedStoichMat = false; allocatedDerivFncts = false;

	int PID = Comm.MyPID();
	unsigned int numProc = Comm.NumProc();
	unsigned int numberOfSpecies = sE->getDim();

	int numLines = 0;
	int* reactionId;
	int* paramId;
	bool seenFirstLine = false;
	createDerivLib(derivFilePath, headerPath, numLines, reactionId, paramId, seenFirstLine, PID);

	//check whether the sbml model was properly defined
	SBMLDocument* sbmlDoc = readSBML(sbmlDocPath);
	unsigned int errors = sbmlDoc->getNumErrors();
	if (errors > 0)
	{
		fprintf(stderr, "There exist errors in the definition of the SBML model, that are listed below. Not able to construct AugmentedModel correctly.\n");
		sbmlDoc->printErrors(std::cerr);
		exit(EXIT_FAILURE);
	}
	model = sbmlDoc->getModel();

	//check whether the number of species are consistent
	if (model->getNumSpecies() != numberOfSpecies)
	{
		fprintf(stderr, "Number of species of the given StateEnum object is inconsistent with the SBML model. Not able to construct AugmentedModel correctly.\n");
		exit(EXIT_FAILURE);
	}

	//derivativeFormula = new ASTNode**[numberOfReactions];
	derivativeFunction = new FunctionPointer*[model->getNumReactions()];
	for (unsigned int i=0; i<model->getNumReactions(); i++)
	{
		//derivativeFormula[i] = new ASTNode*[model->getNumParameters()];
		derivativeFunction[i] = new FunctionPointer[model->getNumParameters()];
		for (unsigned int j=0; j<model->getNumParameters(); j++)
		{
			//derivativeFormula[i][j] = SBML_parseL3Formula("0");
			derivativeFunction[i][j] = &zeroFunction;
		}

		if (PID == 0 && !isMuteOutput) std::cout << "reaction " << i << " is " << model->getReaction(i)->getName() << std::endl;
	}
	allocatedDerivFncts = true;

	params = new double[model->getNumParameters()];
	for (unsigned int i=0; i<model->getNumParameters(); i++)
	{
		if (resetParamsArray != NULL)
		{
			try
			{
				params[i] = resetParamsArray[i];
			}
			catch (...)
			{
				fprintf(stderr, "resetParamsArray does not contain all parameters\n");
				exit(EXIT_FAILURE);
			}
		}
		else
		{
			params[i] = model->getParameter(i)->getValue();
		}

		if (PID == 0 && !isMuteOutput) std::cout << "param " << i << " is " << params[i] <<
										"\t(" << model->getParameter(i)->getName() << ")" << std::endl;
	}

	if (PID == 0 && !isMuteOutput)
	{
		for (unsigned int i=0; i<model->getNumSpecies(); i++)
		{
			std::cout << "species " << i << " is " << model->getSpecies(i)->getName() << std::endl;
		}
	}

	if (!fillDerivativeFunctions(numLines, reactionId, paramId, seenFirstLine)
			&& !isMuteOutput) std::cout << "WARNING: Could not fill derivativeFunctions!\n";
	//if (!fillDerivativeFormulas(derivativeFormulasPath)) std::cout << "WARNING: Could not fill derivativeFormulas!\n";
	if (!fillStoichMat(PID) && !isMuteOutput) std::cout << "WARNING: Could not fill stoichMat!\n";

	matSize = computeMatSize(numberOfSpecies);
	unsigned int numAddStates = numProc - (matSize % numProc);
	if (matSize % numProc != 0)
	{
		matSize += numAddStates;
		if (PID == 0 && !isMuteOutput) std::cout << "enlarged state space by " << numAddStates << " additional states\n";

		stateEnum->setMatSize(matSize);
	}

	if (designatedIndex < 0 || designatedIndex >= matSize)
	{
		fprintf(stderr, "designated state is not within state space\n");
		exit(EXIT_FAILURE);
	}
}

AugmentedModel::~AugmentedModel(void)
{
	if (allocatedDerivFncts)
	{
		for (unsigned int i=0; i<model->getNumReactions(); i++)
		{
			delete[] derivativeFunction[i];
			//delete[] derivativeFormula[i];
		}
		delete[] derivativeFunction;
		//delete[] derivativeFormula;
	}

	if (allocatedStoichMat)
	{
		for (unsigned int i=0; i<model->getNumReactions(); i++) delete[] stoichMat[i];
		delete[] stoichMat;
	}

	delete[] params;
/*
	if (hasSpeciesCountArray)
	{
		for (int i=0; i<model->getNumSpecies(); i++)
		{
			delete[] countOfSpeciesAtState[i];
		}
		delete[] countOfSpeciesAtState;
	}
*/
}

// method written by Ankit Gupta
/*
unsigned int AugmentedModel::sumint(int k){
	return (k*(k+1))/2;
}
*/

// method written by Ankit Gupta
/*
unsigned int AugmentedModel::CantorPairing(unsigned int * state, int size)
{
	int y1 = 0;
	int y2 = 0;
	if (size == 1){
		return state[0];
	}
	else {
		y2 = state[size - 1];
		y1 = CantorPairing(state,size - 1);
		y2 = y1 + y2;
		return sumint(y2) + y1;
	}
}
*/

// method written by Ankit Gupta
/*
void AugmentedModel::UnpairCantor(unsigned int z, unsigned int* x, int size)
{
	double v;
	int v1;

	if (size == 1){
		x[0] = z;
	}
	else{
		v = sqrt(1 + 8*z) -1;
		v = floor(0.5*v);
		v1 = sumint(v);
		UnpairCantor(z - v1, x, size - 1);
		x[size -1] = v1 + v - z;
	}
}
*/


// cantor pairing function
/*
unsigned int AugmentedModel::phi(unsigned int* x)
{
	return CantorPairing(x, model->getNumSpecies()) - offset;
}
*/

/*
// the n-degree generalized Cantor pairing function according to Meri Lisi 2007, p.58
unsigned int AugmentedModel::phiLisi(unsigned int* x)
{
	unsigned int numSpecies = model->getNumSpecies();
	unsigned int z[numSpecies];
	unsigned int sum = 0;
	for (unsigned int i=0; i<numSpecies; i++)
	{
		sum += x[i];
		z[i] = sum;
	}

	unsigned int result = 0;
	for (unsigned int h=1; h<=numSpecies; h++)
	{
		unsigned int product = 1;
		for (unsigned int j=0; j<h; j++)
		{
			product *= z[h-1] + j;
		}

		unsigned int factorial = 1;
		for (unsigned int j=1; j<=h; j++)
		{
			factorial *= j;
		}

		result += product / factorial;
	}

	return result;
}
*/

// inverse cantor pairing function
/*
bool AugmentedModel::phiInv(unsigned int z, unsigned int* x)
{
	UnpairCantor(z + offset, x, model->getNumSpecies());
	return true;
}
*/

double* AugmentedModel::createResetParamsArr(std::string& resetParams)
{
	if (!resetParams.empty())
	{
		std::vector<std::string> params = split(resetParams, ',');

		if (params.size() != model->getNumParameters())
		{
			fprintf(stderr, "could not parse reset parameters\nmake sure to respecify ALL parameter values (and not more or less than all parameters!)\n");
			exit(EXIT_FAILURE);
		}

		double* resetParamsArr; resetParamsArr = new double[params.size()];

		for (unsigned int i=0; i<params.size(); i++)
		{
			try
			{
				resetParamsArr[i] = std::stod(params[i]);
			}
			catch(...)
			{
				fprintf(stderr, "could not parse reset parameters\nmake sure it's a comma-separated string of double values\n");
				exit(EXIT_FAILURE);
			}
		}

		return resetParamsArr;
	}
	else
	{
		return NULL;
	}
}

void AugmentedModel::nextState(unsigned int* x)
{
	stateEnum->nextState(x);
}

//TODO: compute derivatives in an automatic fashion (problem: not necessarily possible in general => probably won't do it)
double AugmentedModel::lamdaKderivOmegaI(int reactionId, int paramId, unsigned int* x)
{
	return derivativeFunction[reactionId][paramId](params, x);
	//return evaluate(derivativeFormula[reactionId][paramId], x);
}

int AugmentedModel::getStoichMatAt(int i, int j)
{
	return (int) stoichMat[i][j];
}

unsigned int AugmentedModel::getNumParameters(void)
{
	return model->getNumParameters();
}

unsigned int AugmentedModel::getNumReactions(void)
{
	return model->getNumReactions();
}

unsigned int AugmentedModel::getMatSize(void)
{
	return matSize;
}

unsigned int AugmentedModel::getDesignatedState(void)
{
	return designatedIndex;
}

unsigned int AugmentedModel::getNumSpecies(void)
{
	return model->getNumSpecies();
}

int AugmentedModel::getLinearIndex(unsigned int* y)
{
	return stateEnum->phi(y);
}

double AugmentedModel::getParam(int i)
{
	return params[i];
}

void AugmentedModel::resetStateArray(unsigned int* x)
{
	stateEnum->resetStateArray(x);
}

bool AugmentedModel::createQ(Epetra_CrsMatrix* &Qt, Epetra_Vector* &invPropSum, Epetra_Map*  &Map, Epetra_Comm &Comm)
{
	bool scaleDiagToNegOne = true;
	unsigned int numSpecies = model->getNumSpecies();
	unsigned int numReactions = model->getNumReactions();

	for (unsigned int k=0; k<numReactions; k++)
	{
		if (!model->getReaction(k)->isSetKineticLaw())
		{
			fprintf(stderr, "no kinetic law is set for reaction \"%s\" in sbml-file\n", model->getReaction(k)->getId().c_str());
			exit(EXIT_FAILURE);
		}
	}

	Map = new Epetra_Map((int) matSize, 0, Comm);
	Qt = new Epetra_CrsMatrix(Copy, *Map, numReactions+1);
	invPropSum = new Epetra_Vector(*Map);

	unsigned int x[numSpecies];
	stateEnum->resetStateArray(x);

	unsigned int y[numSpecies];
	double propensity[numReactions];
	double minusOne = -1.0;
	double curProp;
	unsigned int yID;

	for (int i=0; i<matSize; i++)
	{
		//phiInv(i, x);
		double propSum = 0;
		//printArray(x,numSpecies);
		//std::this_thread::sleep_for(std::chrono::milliseconds(100));

		for (unsigned int k=0; k<numReactions; k++)
		{
			propensity[k] = evaluate(model->getReaction(k)->getKineticLaw()->getMath(), x);
			propSum += propensity[k];
		}

		if (Map->MyGID(i))
		{
			if (scaleDiagToNegOne)
			{
				Qt->InsertGlobalValues(i, 1, &minusOne, &i);
				(*invPropSum)[Map->LID(i)] = 1.0 / propSum;
			}
			else
			{
				double negPropSum = -1 * propSum;
				Qt->InsertGlobalValues(i, 1, &negPropSum, &i);
				(*invPropSum)[Map->LID(i)] = 1.0;
			}
		}

		double outgoingSum = 0;
		for (unsigned int k=0; k<numReactions; k++)
		{
			curProp = propensity[k];
			if (scaleDiagToNegOne) curProp /= propSum;
			if (curProp > 0)
			{
				bool allNewCountsGreaterZero = true;
				for (unsigned int j=0; j<numSpecies; j++)
				{
					int newCount = x[j] + stoichMat[k][j];
					y[j] = newCount;

					//if a "new count" is negative, then the corresponding state is not in the full state space
					//(thus it is not in the truncated state space)
					if (newCount < 0) allNewCountsGreaterZero = false;
				}

				yID = stateEnum->phi(y);
				if (allNewCountsGreaterZero && stateEnum->isInsideStateSpace(y)) //if y is in the truncated state space
				{
					if (Qt->MyGlobalRow((int) yID))
						Qt->InsertGlobalValues(yID, 1, &curProp, &i);
				}
				else
				{
					outgoingSum += curProp;
				}
			}
		}

		if (outgoingSum > 0 && Qt->MyGlobalRow((int) designatedIndex))
			Qt->InsertGlobalValues(designatedIndex, 1, &outgoingSum, &i);

		stateEnum->nextState(x);
	}

	Comm.Barrier();
	Qt->FillComplete();

	//EpetraExt::RowMatrixToMatlabFile("./myQt.dat", *Qt); //TODO remove
	//EpetraExt::MultiVectorToMatlabFile("./invPropSum.dat", *invPropSum); //TODO remove
	//exit(0); //TODO remove
	return true;
}



double AugmentedModel::computeOutflowRate(Epetra_Vector* pi, Epetra_Vector* invPropSum)
{
	//transform pi
	double* piVals;
	pi->ExtractView(&piVals);
	for (int i=0; i<pi->MyLength(); i++)
	{
		piVals[i] *= (*invPropSum)[i];
	}

	//normalize pi
	double mySum = 0;
	for (int i=0; i<pi->MyLength(); i++)
		mySum += (*pi)[i];
	double scaleFact = 0;
	pi->Comm().SumAll(&mySum, &scaleFact, 1);
	pi->Scale(1/scaleFact);


	double propSum = 0;
	unsigned int numSpecies = model->getNumSpecies();
	unsigned int numReactions = model->getNumReactions();
	double propensity[numReactions];
	double curProp;

	unsigned int y[numSpecies];
	unsigned int x[numSpecies];
	stateEnum->resetStateArray(x);

	double localOutflowRate = 0;
	for (int i=0; i<matSize; i++)
	{
		for (unsigned int k=0; k<numReactions; k++)
		{
			propensity[k] = evaluate(model->getReaction(k)->getKineticLaw()->getMath(), x);
			propSum += propensity[k];
		}

		double outgoingSum = 0;
		for (unsigned int k=0; k<numReactions; k++)
		{
			curProp = propensity[k];

			if (curProp > 0)
			{
				bool allNewCountsGreaterZero = true;
				for (unsigned int j=0; j<numSpecies; j++)
				{
					int newCount = x[j] + stoichMat[k][j];
					y[j] = newCount;

					//if a "new count" is negative, then the corresponding state is not in the full state space
					//(thus it is not in the truncated state space)
					if (newCount < 0) allNewCountsGreaterZero = false;
				}

				if (!allNewCountsGreaterZero || !stateEnum->isInsideStateSpace(y)) //if y is in the truncated state space
				{
					outgoingSum += curProp;
				}
			}
		}

		if (pi->Map().MyGID(i))
		{
			localOutflowRate += outgoingSum * (*pi)[pi->Map().LID(i)];
		}

		stateEnum->nextState(x);
	}

	double outflowRate = 0;
	pi->Comm().SumAll(&localOutflowRate, &outflowRate, 1);

	//unnormalize pi
	pi->Scale(scaleFact);

	//back-transform pi
	pi->ExtractView(&piVals);
	for (int i=0; i<pi->MyLength(); i++)
	{
		piVals[i] /= (*invPropSum)[i];
	}

	return outflowRate;
}


bool AugmentedModel::computeExpectations(std::vector<std::string>& fs, Epetra_Vector* pi, std::ofstream& resultsFile, Epetra_Vector* invPropSum,
											cossmosResults* res, cossmosSetups* setups)
{
	//transform pi
	double* piVals;
	pi->ExtractView(&piVals);
	for (int i=0; i<pi->MyLength(); i++)
	{
		piVals[i] *= (*invPropSum)[i];
	}

	//normalize pi
	double mySum = 0;
	for (int i=0; i<pi->MyLength(); i++)
		mySum += (*pi)[i];
	double scaleFact = 0;
	pi->Comm().SumAll(&mySum, &scaleFact, 1);
	pi->Scale(1/scaleFact);



	unsigned int numSpecies = model->getNumSpecies();

	for (unsigned int fNum=0; fNum<fs.size(); fNum++)
	{
		Epetra_Vector* expectationVec = new Epetra_Vector(*pi);
		fGen::fGenerator f = fGen::fGenerator(fs[fNum], numSpecies);

		unsigned int x[numSpecies];
		stateEnum->resetStateArray(x);
		for (int i=0; i<matSize; i++)
		{
			if (expectationVec->Map().MyGID(i))
			{
				unsigned int localId = expectationVec->Map().LID(i);

				(*expectationVec)[localId] *= f.generate(x);
			}

			stateEnum->nextState(x);
		}

		double mySum = 0;
		for (int i=0; i<expectationVec->MyLength(); i++)
			mySum += (*expectationVec)[i];
		double expectation = 0;
		pi->Comm().SumAll(&mySum, &expectation, 1);

		if (pi->Comm().MyPID() == 0)
		{
			std::stringstream ss; ss << std::setprecision(20) << "E[" << fs[fNum] << "]=" << expectation << std::endl;
			res->getExpecVals()[fNum] = expectation;
			if (!setups->isMuteOutput()) std::cout << ss.str();
			if (setups->isUseResultsFile()) resultsFile << ss.str();
		}
	}


	//unnormalize pi
	pi->Scale(scaleFact);

	//back-transform pi
	pi->ExtractView(&piVals);
	for (int i=0; i<pi->MyLength(); i++)
	{
		piVals[i] /= (*invPropSum)[i];
	}

	return true;
}




bool AugmentedModel::fillStoichMat(int PID)
{
	bool isFirstProc = (PID == 0);
	unsigned int R = model->getNumReactions();
	unsigned int S = model->getNumSpecies();

	stoichMat = new double*[R];
	for (unsigned int i=0; i<R; i++) stoichMat[i] = new double[S];
	allocatedStoichMat = true;

	if (isFirstProc && !isMuteOutput) std::cout << "stoichiometric matrix:\n";

	//for every reaction r
	for (unsigned int i=0; i<R; i++)
	{
		Reaction* r = model->getReaction(i);

		//and every speies s
		for (unsigned int j=0; j<S; j++)
		{
			Species* s = model->getSpecies(j);

			double stoichReactant = 0, stoichProduct = 0;

			for (unsigned int k=0; k<r->getNumReactants(); k++)
			{
				//if reactant k of reaction i is the same as species j
				if (r->getReactant(k)->getSpecies().compare(s->getIdAttribute()) == 0)
				{
					//store the stoichiometry of that reactant
					stoichReactant = r->getReactant(k)->getStoichiometry();
					break;
				}
			}

			for (unsigned int k=0; k<r->getNumProducts(); k++)
			{
				//if product k of reaction i is the same as species j
				if (r->getProduct(k)->getSpecies().compare(s->getIdAttribute()) == 0)
				{
					//store the stoichiometry of that product
					stoichProduct = r->getProduct(k)->getStoichiometry();
				}
			}

			//from the stoichiometry of a product and reactant of the same type the entry
			//of the stoichiometry matrix is computed as the difference between those two
			stoichMat[i][j] = stoichProduct - stoichReactant;
			if (isFirstProc && !isMuteOutput) std::cout << stoichMat[i][j] << "\t";
		}
		if (isFirstProc && !isMuteOutput) std::cout << std::endl;
	}

	return true;
}

//TODO: enable user to define code after the derivFun-functions (is that necessary?!?)
bool AugmentedModel::createDerivLib(const char* derivativeFormulasPath, const char* headerPath, int &numLines,
					int* &reactionId, int* &paramId, bool &seenFirstLine, int PID)
{
	seenFirstLine = false;
	numLines = 0;
	bool isFirstProc = (PID == 0);

	std::ifstream infile(derivativeFormulasPath);

	std::string s("");
	if (isFirstProc) s = "./derivativeFormulas.cpp";
	std::ofstream outfile(s);

	if (!infile.is_open())
	{
		fprintf(stderr, "%s\n", "could not find/open file containing the reaction rate derivatives");
		exit(EXIT_FAILURE);
	}

	if (isFirstProc && !outfile.is_open())
	{
		fprintf(stderr, "%s\n", "could not open the output file in createDerivLib()");
		exit(EXIT_FAILURE);
	}

	if (isFirstProc)
	{
		std::string hStr(headerPath);
		if (!hStr.empty())
		{
			std::string header;
			header = readHeader(headerPath);
			outfile << header.c_str();
		}
	}

	outfile << "extern \"C\" {";

	std::string line;
	while (std::getline(infile, line))
	{
		std::istringstream iss(line);

		if (!seenFirstLine)
		{
			int numFunctions = 0;
			iss >> numFunctions;
			reactionId = new int[numFunctions];
			paramId = new int[numFunctions];
			seenFirstLine = true;
			continue;
		}

		std::string formula;
		iss >> reactionId[numLines] >> paramId[numLines] >> formula;

		if (isFirstProc) outfile << "\ndouble derivFun" << numLines << "(double* p, unsigned int* x){\n\treturn " << formula << ";\n}\n";

		numLines++;
	}

	outfile << "};";

	infile.close();

	if (isFirstProc)
	{
		outfile.close();

		std::stringstream ss;
		//TODO the compiler and options should be customizable by the user
		ss  << "g++ derivativeFormulas.cpp -o derivativeFormulas_" << name << ".so -shared -fPIC";
		system(ss.str().c_str()); //note: system() generally returns the status code of the called command, but that status code depends on the implementation of g++; therefore I don't check here whether the g++ code was successful
		if (!isMuteOutput) std::cout << "created shared derivative function library\n";
	}

	return true;
}

bool AugmentedModel::fillDerivativeFunctions(int &numLines, int* &reactionId, int* &paramId, bool &seenFirstLine)
{
	void *handle;
	int it = 0;
	const int waitTime = 10;
	const int maxIt = 6000; //wait for maximally 1 minute (6000*10 milliseconds)

	std::stringstream ss;
	ss  << "./derivativeFormulas_" << name << ".so";

	do
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));
		handle = dlopen(ss.str().c_str(), RTLD_LAZY);
		it++;
	} while (!handle && it < maxIt);

	if (!handle) {
		fprintf(stderr, "%s\n", dlerror());
		exit(EXIT_FAILURE);
	}

	char* error;
	for (int i=0; i<numLines; i++)
	{
		dlerror();

		std::stringstream ss;
		ss  << "derivFun" << i;
		//std::cout << ss.str().c_str() << std::endl;
		derivativeFunction[reactionId[i]][paramId[i]] = (double(*)(double* param, unsigned int* x)) dlsym(handle, ss.str().c_str());

		if ((error = dlerror()) != NULL)  {
			fprintf(stderr, "%s\n", error);
			exit(EXIT_FAILURE);
		}
	}

	//std::cout << "filled derivative functions\n";
	if (seenFirstLine) {delete[] reactionId; delete[] paramId;}
	return true;
}

//TODO: implement all possible cases (see http://sbml.org/Software/libSBML/5.15.0/docs//cpp-api/class_a_s_t_node.html)
//TODO: find out how indicator function is realized in AST and implement it!
double AugmentedModel::evaluate(const ASTNode* n, unsigned int* x)
{
	ASTNodeType_t type = n->getType();
	double retVal;
	const char* name;
	//int speciesId;
	std::smatch m;
	//std::regex ex("^spec::x\\d$");

	switch(type)
	{
	case AST_INTEGER:
		return (double) n->getInteger();
	case AST_REAL:
		return n->getReal();
	case AST_REAL_E:
		return n->getReal();
	case AST_RATIONAL:
		return n->getReal();

	case AST_MINUS:
		return evaluate(n->getLeftChild(), x) - evaluate(n->getRightChild(), x);
	case AST_DIVIDE:
		return evaluate(n->getLeftChild(), x) / evaluate(n->getRightChild(), x);
	case AST_POWER:
		return pow(evaluate(n->getLeftChild(), x), evaluate(n->getRightChild(), x));
	case AST_FUNCTION_POWER:
		return pow(evaluate(n->getLeftChild(), x), evaluate(n->getRightChild(), x));

	case AST_TIMES:
		retVal = 1.0;
		for (unsigned int i=0; i<n->getNumChildren(); i++)
			retVal *= evaluate(n->getChild(i), x);
		return retVal;

	case AST_PLUS:
		retVal = 0.0;
		for (unsigned int i=0; i<n->getNumChildren(); i++)
			retVal += evaluate(n->getChild(i), x);
		return retVal;

	case AST_NAME:
		name = n->getName();

		//TODO: use hashing to avoid looping through all parameters and species each time
		for (unsigned int i=0; i<model->getNumParameters(); i++)
		{
			if (model->getParameter(i)->getId().compare(name) == 0) //if AST_NAME is equal to parameter i's name
			{
				return params[i];
			}
		}

		for (unsigned int i=0; i<model->getNumSpecies(); i++)
		{
			if (model->getSpecies(i)->getId().compare(name) == 0) //if AST_NAME is equal to species i's name
			{
				return x[i];
			}
		}

		fprintf(stderr, "unhandled name \"%s\" encountered in function evaluate()\n", name);
		exit(EXIT_FAILURE);

	case AST_FUNCTION_PIECEWISE:
	{
		int numChilds = n->getNumChildren();
		for (int i=0; i<numChilds; i=i+2)
		{
			if (i+1 < numChilds && evaluate(n->getChild(i+1), x) == 0) //if the condition on this piece of the piecewise function evaluates to true
				return evaluate(n->getChild(i), x);	//return the evaluation of the child that belongs to this condition
		}

		if (numChilds%2 != 0) //if there is an "otherwise"-child
		{
			return evaluate(n->getChild(numChilds-1), x); //return the evaluation of the "otherwise"-child
		}
		else
		{
			fprintf(stderr, "no condition of the piecewise function definition did apply!");
			exit(EXIT_FAILURE);
		}
	}

	case AST_RELATIONAL_GT:
		if (evaluate(n->getChild(0), x) > evaluate(n->getChild(1), x))
			return 0;
		else
			return -1;

	case AST_RELATIONAL_LT:
		if (evaluate(n->getChild(0), x) < evaluate(n->getChild(1), x))
			return 0;
		else
			return -1;

	case AST_RELATIONAL_EQ:
		if (evaluate(n->getChild(0), x) == evaluate(n->getChild(1), x))
			return 0;
		else
			return -1;

	case AST_RELATIONAL_GEQ:
		if (evaluate(n->getChild(0), x) >= evaluate(n->getChild(1), x))
			return 0;
		else
			return -1;

	case AST_RELATIONAL_LEQ:
		if (evaluate(n->getChild(0), x) <= evaluate(n->getChild(1), x))
			return 0;
		else
			return -1;

	case AST_RELATIONAL_NEQ:
		if (evaluate(n->getChild(0), x) != evaluate(n->getChild(1), x))
			return 0;
		else
			return -1;

	default:
		fprintf(stderr, "%s\n", "default statement encountered in function evaluate(): you might need to implement an additional switch case for some ASTNodeType_t's");
		exit(EXIT_FAILURE);
	}
}

double AugmentedModel::zeroFunction(double* param, unsigned int* x)
{
	return 0.0;
}

unsigned int AugmentedModel::computeMatSize(unsigned int numSpecies)
{
	unsigned int xMax[numSpecies];
	stateEnum->setLastStateArray(xMax);
	unsigned int matSize = stateEnum->phi(xMax) + 1;
	return matSize;
}

StateEnum* AugmentedModel::getStateEnum() {
	return stateEnum;
}

void AugmentedModel::setStateEnum(StateEnum* sE, Epetra_Comm &comm)
{
	stateEnum = sE;

	unsigned int numberOfSpecies = stateEnum->getDim();
	int PID = comm.MyPID();
	unsigned int numProc = comm.NumProc();

	if (model->getNumSpecies() != numberOfSpecies)
	{
		fprintf(stderr, "Number of species of the given StateEnum object is inconsistent with the SBML model. Not able to (re-)set the state space enumeration correctly.\n");
		exit(EXIT_FAILURE);
	}

	matSize = computeMatSize(numberOfSpecies);
	unsigned int numAddStates = numProc - (matSize % numProc);
	if (matSize % numProc != 0)
	{
		matSize += numAddStates;
		if (PID == 0 && !isMuteOutput) std::cout << "enlarged state space by " << numAddStates << " additional states\n";

		stateEnum->setMatSize(matSize);
	}
}

void AugmentedModel::resetParams(double* params)
{
	for (int i=0; i<model->getNumParameters(); i++)
	{
		this->params[i] = params[i];
	}
}
