#include "readFunctions.h"

int readMatrixRowsFromFile(const char* path, int &entriesPerRow)
{
	std::ifstream infile(path);

	std::string line;
	while (std::getline(infile, line))
	{
		if (line[0] != '%')
		{
			std::istringstream iss(line);
			int rows, cols, elems;
			if (!(iss >> rows >> cols >> elems))
			{
				std::cout << "could not determine matrix number of rows" << std::endl;
				return -1;  // error
			}

			entriesPerRow = round(((double) elems) / ((double) rows));
			return rows;
		}
	}

	std::cout << "could not determine matrix number of rows" << std::endl;
	return -1;
}

//unused function
//bool fillMatrixByFile(TMatrix &A, TMatrix &eye, const char* path)
//{
//	std::ifstream infile(path);
//
//	bool seenFirstLine = false;
//	int numRows;
//	std::string line;
//	while (std::getline(infile, line))
//	{
//		if (line[0] != '%')
//		{
//			std::istringstream iss(line);
//			int row, col; std::string value;
//			iss >> row >> col;
//
//			if (!seenFirstLine)
//			{
//				numRows = row;
//				seenFirstLine = true;
//				continue;
//			}
//
//			row--; col--;
//			if (iss) iss >> value; //if a value is specified, read it as well
//
//			double dblVal;
//			if (value.empty())
//				//assign a random value between 0 and 1
//				dblVal = ((double) rand() / (RAND_MAX));
//			else
//				//convert the value from string to double
//				dblVal = std::stod(value, NULL);
//
//
//			if (A->MyGlobalRow(row))
//				A->InsertGlobalValues(row, 1, &dblVal, &col);
//
//		}
//	}
//
//	//create small dummy eye with dimension 1
//	double dblOne = 1.0;
//	int i=0;
//    eye->InsertGlobalValues(i, 1, &dblOne, &i); //can I get rid of eye? (no need to => this is unused function)
//
//	A->FillComplete();
//	eye->FillComplete();
//
//	return true;
//}


bool readBFsFile(const char* desiredFpath, fGen::fGenerator** &fGens, int &numDesiredF, int numSpecies)
{
	std::ifstream infile(desiredFpath);

	std::string* desiredF;
	bool seenFirstLine = false;
	std::string line;
	int numLines = 0;

	if (!infile.is_open()) return false;

	while (std::getline(infile, line))
	{
		std::istringstream iss(line);

		if (!seenFirstLine)
		{
			int numExpectSpecies;
			iss >> numDesiredF >> numExpectSpecies;

			if (numExpectSpecies != numSpecies)
			{
				fprintf(stderr, "%s\n", "ERROR: the number of species specified in desiredF file is not as expected");
				exit(EXIT_FAILURE);
			}

			desiredF = new std::string[numDesiredF];
			fGens = new fGen::fGenerator*[numDesiredF];

			seenFirstLine = true;
		}
		else
		{
			iss >> desiredF[numLines];

			numLines++;
		}
	}

	for (int i=0; i<numDesiredF; i++)
		fGens[i] = new fGen::fGenerator(desiredF[i], numSpecies);

	if (numLines > 0) delete[] desiredF;

	if (numLines != numDesiredF) return false;
	return true;
}

std::string readHeader(const char* headerFilePath)
{
	std::string line,text;
	std::ifstream in(headerFilePath);

	if (!in.is_open()) return std::string("");
	while(std::getline(in, line))
	{
		text += line + "\n";
	}

	return text;
}

std::vector<std::string> split(const std::string& s, char delimiter)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}
