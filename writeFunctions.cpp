#include "writeFunctions.h"

bool writeArrayToMatlabFile(const char* path, double* arr, int length)
{
	std::ofstream outfile(path);
	if (!outfile.is_open()) return false;

	for (int i=0; i<length; i++)
	{
		outfile << std::setprecision(16) << arr[i] << "\n";
	}
	outfile.close();

	return true;
}

bool write2DarrayToMatlabFile(const char* path, double** arr, int length1, int length2)
{
	std::ofstream outfile(path);
	if (!outfile.is_open()) return false;

	for (int i=0; i<length1; i++)
	{
		for (int j=0; j<length2; j++)
		{
			outfile << arr[i][j] << " ";
		}
		outfile << "\n";
	}
	outfile.close();

	return true;
}

bool printArray(double* arr, int length)
{
	for (int i=0; i<length; i++)
	{
		std::cout << arr[i] << "\t";
	}
	std::cout << "\n";

	return true;
}

bool printArray(int* arr, int length)
{
	for (int i=0; i<length; i++)
	{
		std::cout << arr[i] << "\t";
	}
	std::cout << "\n";

	return true;
}

bool printArray(unsigned int* arr, int length)
{
	//int sum = 0;
	for (int i=0; i<length; i++)
	{
		//sum += arr[i];
		std::cout << arr[i] << "\t";
	}
	std::cout /*<< sum */<< "\n";

	return true;
}

bool print2DArray(double** arr, int len1, int len2)
{
	for (int i=0; i<len1; i++)
	{
		for (int j=0; j<len2; j++)
		{
			std::cout << arr[i][j] << "\t";
		}
		std::cout << "\n";
	}

	return true;
}
