created shared derivative function library
param 0 is 50
param 1 is 4
param 2 is 0.5
param 3 is 0.2
stoichiometric matrix:
1	0	
0	1	
-1	0	
0	-1	
computing sensitivities for genex network
Process 0 has 5044501 of 5044501 elements.
E[x1]=100.00000000000633804
E[x2]=2000.0000000002601155
Computing sensitivities for g=x1
the number of monomials is 2
x2 x1 
total elapsed seconds in BFM is: 0.902975
approximation error for the basis function method is: 4.39926e-06
sensitivity for parameter 0 is: 2.0000019126008359294
sensitivity for parameter 1 is: 2.2506596605943682027e-05
sensitivity for parameter 2 is: -200.00019126009215142
sensitivity for parameter 3 is: -0.0004501318858656655356
Computing sensitivities for g=x2
the number of monomials is 2
x2 x1 
total elapsed seconds in BFM is: 0.878776
approximation error for the basis function method is: 8.94234e-05
sensitivity for parameter 0 is: 40.000038875897679702
sensitivity for parameter 1 is: 500.00045843179157146
sensitivity for parameter 2 is: -4000.0038875899840605
sensitivity for parameter 3 is: -10000.009168634989692

This cossmosResults object holds the following results:

expectations:
100, 2000

sensitivities:
function 0:
2, 2.251e-05, -200, -0.0004501
function 1:
40, 500, -4000, -1e+04


