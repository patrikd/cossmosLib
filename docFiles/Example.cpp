//include the cossmos header from the cossmos library
#include "cossmos.h"

int main(int argc, char *argv[])
{
	//initialize the MPI execution environment
	MPI_Init(&argc, &argv);

	//now all parameters required for the augmented model definition are defined
	Epetra_MpiComm comm(MPI_COMM_WORLD); //a communicator object that makes use of the MPI execution environment to distribute computations among different processors
	unsigned int designatedIndex = 1000; //the designated index (required for the sFSP algorithm)
	const char* SBMLfilePath = "./genexClean.sbml"; //the path to the SBML model defining the reaction network
	const char* headerFilePath = ""; //an empty string, because no header file is required for the augmented gene expression network model
	const char* derivFilePath = "./derivativeFunctions.txt"; //the path to a file containing the partial derivatives of the reaction rate functions
	int numSpecies = 2; //the number of species in the reaction network
	double resetParams[4] = {50.0, 4.0, 0.5, 0.2}; //the values of the reaction network parameters
	int cutOff[2] = {900, 3300}; //the cut-off values defining the state space truncation of the reaction network model
	StateEnum* stateEnum = new CantorLisi(cutOff, numSpecies); //the state space enumeration and truncation object CantorLisi
	const char* netName = "genex"; //the arbitrary network name

	//construct the augmented model
	AugmentedModel augModel(netName, resetParams, designatedIndex, stateEnum, SBMLfilePath, derivFilePath, headerFilePath, comm, false);

	//uncomment this to print each state inside the truncated state space (useful for debugging custom classes derived from the StateEnum class)
	//int sleepTime = 1000;
	//augModel.printStateSpace(sleepTime);

	//create a cossmosSetups object by calling this constructor
	//it sets all cossmosSetups members to reasonable default values
	cossmosSetups setups("x1,x2", "x1,x2", numSpecies);

	//one can modify those default values at will (but one does not have to)
	setups.setNetName(netName);
	//setups.setComputePi(false);
	//setups.setStorePi(false);

	//create an cossmos algorithm object using the defined setups
	cossmosAlgo algo(&setups);

	//estimate the steady-state parametric sensitivities of the specified augmented model by calling the cossmos function of the cossmos algorithm object
	cossmosResults* res = algo.cossmos(augModel, comm);

	//print the cossmos results (note that the cossmosResults object contains only correct results on the processor with ID 0, see description of cossmosAlgo::cossmos())
	if (comm.MyPID() == 0) res->print();

	//free the memory allocated during the construction of stateEnum
	delete stateEnum;

	//terminate the MPI execution environment
	MPI_Finalize();

	return 0;
}
