#!/bin/bash

cmake \
-D CMAKE_BUILD_TYPE=RELEASE \
-D Trilinos_ENABLE_ALL_PACKAGES=OFF \
-D Trilinos_ENABLE_Epetra=ON \
-D Trilinos_ENABLE_Teuchos=ON \
-D Trilinos_ENABLE_Amesos=ON \
-D Trilinos_ENABLE_AztecOO=ON \
-D Trilinos_ENABLE_Anasazi=ON \
-D TPL_ENABLE_MPI=ON \
-D MPI_BASE_DIR=/opt/intel/impi/2018.3.222/intel64/lib \
-D CMAKE_C_COMPILER=/opt/intel/impi/2018.3.222/intel64/bin/mpicc \
-D CMAKE_CXX_COMPILER=/opt/intel/impi/2018.3.222/intel64/bin/mpicxx \
-D CMAKE_Fortran_COMPILER=/opt/intel/impi/2018.3.222/intel64/bin/mpif90 \
-D TPL_ENABLE_BLAS:BOOL=ON \
-D BLAS_LIBRARY_NAMES="mkl_intel_lp64;mkl_sequential;mkl_core" \
-D BLAS_LIBRARY_DIRS=/opt/intel/compilers_and_libraries_2018.3.222/linux/mkl/lib/intel64 \
-D TPL_ENABLE_LAPACK:BOOL=ON \
-D LAPACK_LIBRARY_NAMES="" \
-D LAPACK_LIBRARY_DIRS=/opt/intel/compilers_and_libraries_2018.3.222/linux/mkl/lib/intel64 \
-D TPL_ENABLE_SuperLUDist:BOOL=ON \
-D SuperLUDist_INCLUDE_DIRS:FILEPATH=$HOME/superlu_dist_5.3.0/SuperLU_DIST_5.3.0/SRC \
-D SuperLUDist_LIBRARY_DIRS:FILEPATH=$HOME/superlu_dist_5.3.0/SuperLU_DIST_5.3.0/lib \
-D TPL_ENABLE_ParMETIS:BOOL=ON \
-D TPL_ParMETIS_LIBRARIES="/usr/local/lib/libparmetis.a" \
-D TPL_ParMETIS_INCLUDE_DIRS="/usr/local/include"\
-D CMAKE_INSTALL_PREFIX=$HOME/TrilinosBuild \
../trilinos-12.12.1-Source
