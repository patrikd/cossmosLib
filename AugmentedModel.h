#ifndef AUG_MODEL_H_INCLUDED
#define AUG_MODEL_H_INCLUDED

#include "cossmosResults.h"
#include "commonHeaders.h"
#include "fGenerator.h"
#include "readFunctions.h"
#include "StateEnum.h"

typedef double(*FunctionPointer)(double* param, unsigned int* x);

/*!
 * \brief On top of the SBML model, that models reaction networks for general purposes, for the cossmosLib we need additional, problem specific
 *  information to estimate the steady-state parametric sensitivities for reaction networks. The fusion of the SBML model and this additional
 *  information we call the augmented model.
 *
 *  The additional information contained in the augmented model is the following: state space enumeration, state space truncation,
 *  designated state index (for sFSP) and partial derivatives of reaction rate functions
 */
class AugmentedModel
{
private:
	Model* model;
	const char* name;
	StateEnum* stateEnum;
	double** stoichMat;
	bool allocatedStoichMat, allocatedDerivFncts, isMuteOutput;
	unsigned int designatedIndex;
	unsigned int matSize;
//	ASTNode*** derivativeFormula;
	FunctionPointer** derivativeFunction;
	double *params;
//	int** countOfSpeciesAtState;
//	bool hasSpeciesCountArray;

public:
	/*! \brief
	 * \param n A string specifying the network name. It is mainly used to name the shared partial derivatives library file which is generated
	 * and compiled automatically during the call to this constructor. The most convenient way to set this parameter is to use the function
	 * cossmosSetups::getNetName().c_str().
	 *
	 * \param sbmlFilePath A string specfying the absolute or relative path to an SBML file describing the underlying reaction network model used
	 * for this augmented model. The structure and syntax of this file was described in Section 4.2 of the thesis.
	 *
	 * \param desigIdx A non-negative integer specifying the one-dimensional index \f$d\f$ of the designated state \f$x_d\f$ (compare to Section 2.4 in
	 * the thesis) according to the enumeration function StateEnum::phi(). E.g. if the default state space enumeration CantorLisi is used,
	 * the enumeration function corresponds to the phi() function from Equation 4.1 in the thesis. The index \f$d\f$ must be between \f$0\f$ and
	 * \f$n-1\f$ where \f$n\f$ is the number of states in the truncated state space. This is because the indexing of states is automatically
	 * shifted such that the first state (the one returned by StateEnum::resetStateArray()) has index 0 and the last state
	 * (the one returned by StateEnum::setLastStateArray()) has index n-1.
	 *
	 * \param stateEnum Pointer to an object of a class that implements the StateEnum interface. The cossmos framework provides two such classes:
	 * CantorLisi and RectEnum2D. This parameter defines the state space enumeration and truncation of the augmented model.
	 *
	 * \param derivFilePath A string specifying the absolute or relative path to a partial derivative file describing all partial derivatives
	 * \f$\frac{\partial}{\partial \theta_j} \lambda_k(\theta, x)\f$ of reaction rate functions that are not equal to zero. The structure and
	 * syntax of this file is described in Section 4.5 of the thesis.
	 *
	 * \param headerFilePath A string specifying the absolute or relative path to the header file that is associated to the partial derivative
	 * file specified under \a derivFilePath. The structure and syntax of this file was described in Section 4.5 of the thesis. This parameter
	 * can be set to the empty string "" if no header file is required.
	 *
	 * \param resetParams A pointer to an array of double values (re-)specifying all parameter values
	 * \f$\theta = [\theta_1 , \dots , \theta_P]^T\f$ in the natural order from 1 to P . Make sure not to specify more or less then P parameter
	 * values. Doing so would cause the program to exit with an error. By handing over the NULL pointer as resetParams, the parameters specified in
	 * the SBML file are used. This redundant way of (re-)setting the reaction network model parameters was introduced to simplify the following
	 * use case: If one wants to compute the steady-state parametric sensitivities for the same network type but with changed parameters, one does
	 * not need to modify the respective SBML file. Instead one can use the same SBML file and construct a new AugmentedModel object with the
	 * desired \a resetParams.
	 *
	 * \param Comm The \a Comm object handles the MPI communication between processors. \a Comm is either an \a Epetra_MpiComm object (for
	 * distributed computing) or an \a Epetra_SerialComm object (for serial computing).
	 * Both are classes that are derived from the virtual class \a Epetra_Comm.
	 *
	 * \param muteOutput If set to true, the program doesn't print any information to the standard output. If set to false, the
	 * program prints information to the standard output.
	 */
	//TODO: what if SBML model has reversible (or fast) reactions, will my program still work?
	AugmentedModel(const char* n, double* resetParams, unsigned int desigIdx, StateEnum* stateEnum, const char* sbmlFilePath,
					const char* derivFilePath, const char* headerFilePath, Epetra_Comm &Comm, bool muteOutput);
	~AugmentedModel(void);

	/*! \brief This function acts as an interface to the function StateEnum::nextState(). It is calling the nextState function of the StateEnum
	 * object handed over at this AugmentedModel object's construction.
	 */
	void nextState(unsigned int* x);

	/*!
	 * \returns This function returns the evaluation of the partial derivative (with respect to the parameter specified by \a paramId) of the
	 * reaction rate function (for the reaction specified by \a reactionId) at state \a x.
	 * Or written as a formula: returns\f$\frac{\partial}{\partial \theta_j} \lambda_k(\theta, x)\f$ where \f$k\f$ == \a reactionId and
	 * \f$j\f$ == \a paramId.
	 */
	//TODO: compute derivatives in an automatic fashion (problem: not necessarily possible in general => probably won't do it)
	double lamdaKderivOmegaI(int reactionId, int paramId, unsigned int* x);

	// getters
	/*!
	 * \returns This function returns the entry (indexed by \a i and \a j) of the stoichiometric matrix of this augmented model. Thus the returned value is the change
	 * in the \a j-th species number during reaction \a i.
	 */
	int getStoichMatAt(int i, int j);

	/*!
	 * \returns This function returns the number of parameters in this augmented model.
	 */
	unsigned int getNumParameters(void);

	/*!
	 * \returns This function returns the number of reactions in this augmented model.
	 */
	unsigned int getNumReactions(void);

	/*!
	 * \returns This function returns the one-dimensional index of the designated state that was handed over at this AugmentedModel object's
	 * construction.
	 */
	unsigned int getDesignatedState(void);


	/*!
	 * \returns This function returns the size of the generator matrix (i.e. the size of the state space).
	 */
	unsigned int getMatSize(void);

	/*!
	 * \returns This function returns the number of species in this augmented model.
	 */
	unsigned int getNumSpecies(void);

	/*!
	 * \returns This function returns the value of parameter \f$\theta_i\f$ in this augmented model.
	 */
	double getParam(int i);

	/*!
	 * \returns This function returns the linear index of the state vector \a y according to the state space enumeration function
	 * StateEnum::phi() of the StateEnum object handed over at this AugmentedModel object's construction. Note that the returned linear
	 * index is shifted such that the first state (the one returned by StateEnum::resetStateArray()) has index 0.
	 */
	int getLinearIndex(unsigned int* y);

	/*! \brief This function acts as an interface to the function StateEnum::resetStateArray(). It is calling the resetStateArray function of the
	 * StateEnum object handed over at this AugmentedModel object's construction.
	 */
	void resetStateArray(unsigned int* x);

	/*! \brief This function creates the transpose of the scaled generator matrix \f$ Q_n^{*T} \f$ according to the augmented model information.
	 *
	 * \param[out] Qt After the call to \a createQ this pointer will point to the address (in the heap memory) where the transpose of the scaled
	 * generator matrix \f$ Q_n^{*T} \f$ is located. Note that the heap memory pointed to by this pointer must be freed by the caller of this function!
	 *
	 * \param[out] invPropSum After the call to \a createQ this pointer will point to the address (in the heap memory) where the vector holding the
	 * inverses of the propensity sums for each row of the generator matrix is located. Note that the heap memory pointed to by this pointer must be freed by the
	 * caller of this function!
	 *
	 * \param[out] Map After the call to \a createQ this pointer will point to the address (in the heap memory) where the Epetra_Map object responsible for
	 * all the parallelism in the cossmos framework is located. Note that the heap memory pointed to by this pointer must be freed by the caller of this function!
	 *
	 * \param[in] Comm The \a Comm object handles the MPI communication between processors. \a Comm is either an \a Epetra_MpiComm object (for distributed computing) or an \a Epetra_SerialComm object (for serial computing).
	 *  Both are classes that are derived from the virtual class \a Epetra_Comm.
	 */
	bool createQ(Epetra_CrsMatrix* &Qt, Epetra_Vector* &invPropSum, Epetra_Map*  &Map, Epetra_Comm &Comm);

	/*! \brief This function computes the expectations \f$ E[f_i(X)] \f$ for all functions \f$ f_i \f$ within the set of functions specified by \a fs
	 * under the assumption that the random state vector \f$ X \f$ has the distribution specified by \f$ \pi \f$. The computed expectations are 1) printed
	 * to the standard output, 2) written to the \a resultsFile (if cossmosSetups::setUseResultsFile() was set to true for \a setups) and 3) stored in the given
	 * \a res object.
	 *
	 * \param fs A vector of strings. Each vector element specifies one function \f$ f_i \f$ for which one wants to compute \f$ E[f_i(X)] \f$.
	 *
	 * \param pi Is the pointer returned by cossmosAlgo::computeStationaryDist(). Note that \a pi represents the scaled and transformed stationary
	 * distribution \f$ \hat{\pi}^* \f$. As a first step in computeExpectations() \f$ \pi \f$ is recovered from \f$ \hat{\pi}^* \f$. How this is done is
	 * explained in Section 4.8 of the thesis. Here it is only important to know that in order to recover \f$ \pi \f$ we also require the Epetra_Vector
	 * \a invPropSum.
	 *
	 * \param invPropSum Is the pointer to a Epetra_Vector object that is returned by the call to the generator matrix construction function
	 * (AugmentedModel::createQ())
	 */
	bool computeExpectations(std::vector<std::string>& fs, Epetra_Vector* pi, std::ofstream& resultsFile, Epetra_Vector* invPropSum,
								cossmosResults* res, cossmosSetups* setups);

	/*!
	 * \returns This function returns the pointer to the StateEnum object which was handed to this AugmentedModel object during construction.
	 */
	StateEnum* getStateEnum();

	/*! \brief This function computes the outflow rate of the stationary distribution given by \a pi. It is defined in Equation 3.2 in: "A. Gupta, J. Mikelson, and M. Khammash.
	 * A finite state projection algorithm for the stationary solution of the chemical master equation. The Journal of Chemical Physics, 147(15):154101, 2017."
	 *
	 * \param pi Is the pointer returned by cossmosAlgo::computeStationaryDist(). Note that \a pi represents the scaled and transformed stationary
	 * distribution \f$ \hat{\pi}^* \f$. As a first step in computeOutflowRate() \f$ \pi \f$ is recovered from \f$ \hat{\pi}^* \f$. How this is done is
	 * explained in Section 4.8 of the thesis. Here it is only important to know that in order to recover \f$ \pi \f$ we also require the Epetra_Vector
	 * \a invPropSum.
	 *
	 * \param invPropSum Is the pointer to a Epetra_Vector object that is returned by the call to the generator matrix construction function
	 * (AugmentedModel::createQ())
	 */
	double computeOutflowRate(Epetra_Vector* pi, Epetra_Vector* invPropSum);

	/*! \brief Sets the state space enumeration of the respective augmented model. Use case: If one wants to evaluate different state space truncations
	 * for the same model, one can set the different truncations with this function.
	 *
	 * Note that this function checks whether the number of species in the given state space enumeration is equal to the number of species defined in the
	 * model and terminates the program should that not be the case.
	 *
	 * Further the state space is automatically enlarged, should the size of the given state space truncation divided by the number of processors not be
	 * an integer.
	 *
	 * \param sE The state space enumeration and truncation object that is set.
	 *
	 * \param comm The Epetra communicator that handles the communication between processors.
	 */
	void setStateEnum(StateEnum* sE, Epetra_Comm &comm);

	/*!
	 * \brief Sets the model parameters to the given parameter vector.
	 *
	 * Use case: If one wants to evaluate different parameters for the same model, one can set them with this function.
	 *
	 * \param params Model parameters vector that is set.
	 */
	void resetParams(double* params);

private:
	// cantor pairing function
	//unsigned int phi(unsigned int* x); //currently unused function
	// the n-degree generalized Cantor pairing function according to Meri Lisi 2007, p.58
	//unsigned int phiLisi(unsigned int* x);
	// inverse cantor pairing function
	//bool phiInv(unsigned int z, unsigned int* x); //currently unused function

	// methods written by Ankit Gupta
	//unsigned int sumint(int k);
	//unsigned int CantorPairing(unsigned int * state, int size);
	//void UnpairCantor(unsigned int z, unsigned int* x, int size);

	bool fillStoichMat(int PID);

	//TODO: enable user to define code after the derivFun-functions (is that necessary?!?)
	bool createDerivLib(const char* derivativeFormulasPath, const char* headerPath, int &numLines,
						int* &reactionId, int* &paramId, bool &seenFirstLine, int PID);

	bool fillDerivativeFunctions(int &numLines, int* &reactionId, int* &paramId, bool &seenFirstLine);

	//TODO: implement all possible cases (see http://sbml.org/Software/libSBML/5.15.0/docs//cpp-api/class_a_s_t_node.html)
	//TODO: find out how indicator function is realized in AST and implement it!
	double evaluate(const ASTNode* n, unsigned int* x);

	static double zeroFunction(double* param, unsigned int* x);

	unsigned int computeMatSize(unsigned int numSpecies);

	double* createResetParamsArr(std::string& resetParams);

};

#endif
