#ifndef STATE_ENUM_H_INCLUDED
#define STATE_ENUM_H_INCLUDED

#include "commonHeaders.h"
#include "fGenerator.h"
#include "readFunctions.h"
#include "writeFunctions.h"

/*!
 * \brief This is an abstract class that acts as an interface for several different state space enumerations.
 *
 * Its functions generateFvec() and generateG() are not relevant for the cossmosLib user.
 *
 * cossmosLib is distributed with this state space enumeration interface and several C++ classes (CantorLisi, RectEnum, MixRectCantor)
 * implementing it. The program user has to hand an StateEnum object implementation to the AugmentedModel object when it is
 * constructed.
 */
class StateEnum
{
protected:
	/*! \brief The dimension of the truncated state space. Must be set in the constructor of all classes implementing this abstract class.
	 */
	unsigned int dim;

private:
	void computeMatSize();
	unsigned int matSize;
	bool hasMatSize;

public:
	StateEnum();

	/*! \brief This function creates an Epetra_Vector with the given \a Map that has the form \f$ [f(x_0), \dots, f(x_{n-1})]^T \f$ where
	 * \f$ x_0 \f$ is the first state in the truncated state space, \f$ x_{n-1} \f$ is the last state in the truncated state space and \f$ f \f$
	 * is the function defined by the string argument \a desiredFvec.
	 *
	 *  \param desiredFvec A string specifying the desired function \f$ f \f$. E.g.: \a x1 \f$\rightarrow\f$ \f$\{x_1\}\f$; \a x1,x2 \f$\rightarrow\f$
	 *  \f$\{x_1, x_2\}\f$; \a x1,x2,x1^2,x2^2 \f$\rightarrow\f$ \f$\{x_1, x_2, x_1^2, x_2^2\}\f$; \a 1/x5^2 \f$\rightarrow\f$ \f$\{\frac{1}{x_5^2}\}\f$
	 *  (note: here the index \f$ i \f$ of the state vector \f$ x \f$ signifies that \f$ x_i \in R \f$ is the element in the \f$ i \f$-th
	 *  dimension of \f$ x \f$, whereas above \f$ x_i \in R^{numDims} \f$ is the \f$ i \f$-th state space vector in the truncated state space.)
	 *
	 *  \returns The pointer to the created Epetra_Vector. Note that the heap memory pointed to by this pointer must be freed by the caller of this function!
	 */
	Epetra_Vector* generateFvec(const std::string& desiredFvec, const Epetra_BlockMap &Map);

	/*! \breif This function creates an Epetra_MultiVector (a set of Epetra_Vectors) with the given \a Map that contains the set of basis function vectors
	 * for the basis function method. There are two modes how one can specify the set of basis functions (see also documentation of
	 * cossmosSetups::setUseBFsFile() and Section 4.7.1 in the thesis). If \a useBFsFile is set to true, generateG() uses the file at \a BFsFilePath to
	 * create the set of basis function vectors. If \a useBFsFile is set to false, generateG() uses \a maxDegree to generate a set of basis functions
	 * containing all monomial functions with a maximum degree lower or equal to \a maxDegree.
	 *
	 * \param maxDegreeVector A vector containing the \a maxDegree for each species of this state space.
	 *
	 * \param isMuteOutput If set to true, the program doesn't print any information to the standard output. If set to false, the
	 * program prints information to the standard output.
	 */
	Epetra_MultiVector* generateG(const char* BFsFilePath, int* maxDegreeVector, bool useBFsFile, const Epetra_BlockMap &Map, bool isMuteOutput);

	/*! A pure virtual function that (re-)sets the given state vector \f$ x \f$ to the state with the linear index 0 according to the enumeration
	 * \f$ \phi \f$ (i.e. it sets \f$ x \f$ such that \f$ \phi(x)=0 \f$). We will refer to this state as the "first state".
	 */
	virtual void resetStateArray(unsigned int* x) = 0;

	/*! A pure virtual function that sets the given state \f$ x \f$ to the state with the largest one-dimensional index of all states within
	 * the truncated state space according to the enumeration \f$ \phi \f$.  We will refer to this state as the "last state". Note that the
	 * functions StateEnum::resetStateArray and StateEnum::setLastStateArray are implicitly truncating the full state space.
	 */
	virtual void setLastStateArray(unsigned int* x) = 0;

	/*!	A pure virtual function that sets the given state \f$ x_i \f$ to the successive state \f$ x_{i+1} \f$ according to the enumeration
	 * \f$ \phi \f$. Note that this function must be able to go beyond the last state in the truncation. This is because a call like
	 * StateEnum::nextState(StateEnum::setLastStateArray(\f$ x_i \f$)) must still return a meaningful result, at least for a few further
	 * StateEnum::nextState calls. This is because cossmosLib requires to have an equal number of states assigned to all of the processors
	 * it is executed on. Therefore, if \f$ \frac{state\:space\:size}{\#processors} \notin N \f$ the state space size is automatically
	 * increased until the shown fraction is an integer. Consequently, the truncated state space needs to be extensible by at least
	 * \f$ (\#processors - 1) \f$ states.
	 */
	virtual void nextState(unsigned int* x) = 0;

	/*! A pure virtual function that returns the one-dimensional index \f$i \f$ of the state vector \f$x_i\f$ which is given as an input
	 * parameter. This function effectively implements an enumeration function \f$ \phi : N^M_0 \rightarrow N_0 \f$ which maps an \f$ M \f$-dimensional
	 * vector to a one dimensional index space.
	 */
	virtual unsigned int phi(unsigned int* x) = 0;

	/*! An (already implemented) function that decides whether the given state \f$ x \f$ is within the truncated state space defined by
	 * StateEnum::resetStateArray and StateEnum::setLastStateArray. Note that possible states within the extension of the truncated state
	 * space (as explained under StateEnum::nextState), are considered to be inside the state space as well.
	 *
	 * This function essentially checks whether the one dimensional index \f$ i \f$ of the input vector \f$ x_i \f$ is between the one
	 * dimensional index of the first state, i.e. 0, and the one dimensional index of the last state of the truncated state space according
	 * to \f$ \phi \f$.
	 */
	bool isInsideStateSpace(unsigned int* x);

	/*! Returns the number of species depicted by this state space, i.e. the dimension of this state space.
	 */
	int getDim() const;
	virtual ~StateEnum() {}

	/*! \brief Prints out the whole truncated state space and its enumeration according to the StateEnum object.
	 * Thereby each state, starting from the state with the smallest index, and some state specific information (like the state
	 * vector and the state index) is printed on a separate line. Between the printing of two lines the function sleeps for \a sleepTime milliseconds.
	 */
	void printStateSpace(int sleepTime);

	/*! Sets the state space size (in this function referred to as matSize). This function should not be used by the cossmosLib user.
	 * It is used internally to reset the state space size in the case where \f$ \frac{state\:space\:size}{\#processors} \notin N \f$
	 * (as explained under StateEnum::nextState).
	 */
	void setMatSize(unsigned int matSize);

	/*!
	 * \returns This function returns the number of states in the state space defined by this state space enumeration object.
	 */
	unsigned int getNumStates();
};

/*!
 * \brief This class is derived from StateEnum. It implements the state space enumeration according to a cantor function.
 *
 * Note that all functions it implements receive the reference to a state space vector \f$ \boldsymbol{x} := [x_1 , \dots , x_n ]^T \f$ as input
 * and/or output parameter.
 */
class CantorLisi : public StateEnum
{
private:
	int* cutOff;
	int offset;
	bool isFullyConstructed;

public:
	/*!
	 * \param cutOff An int-array with two entries. The first entry holds the lower cut-off value \f$ c_l \f$ and the second entry hold the
	 * upper cut-off value \f$ c_u \f$.
	 *
	 * \param dim The dimensionality (i.e the number of species) of the truncated state space.
	 */
	CantorLisi(int* cutOff, int dim);
	virtual ~CantorLisi();

	/*!	The state space truncation is done by discarding all the states \f$ \boldsymbol{x} \f$ for which
	 *
	 * \f$ c_l \leq  \sum_{i=1}^{n} x_i \leq c_u \f$
	 *
	 * does not hold for some user defined, lower and upper cut-off values \f$ c_l,c_u \in \mathbb{N}_{0} \f$. Thus the truncated state space
	 * \f$ \mathcal{E}_n \f$ contains only states for which the sum of all present chemical entities, does not go below or above some chosen
	 * bounds. Since further we can easily check that
	 *
	 * \f$ \phi(0, \dots, 0, c_l) = \sum_{h=1}^{n} \left(\frac{1}{h!} \prod_{j=0}^{h-1} [(c_l \cdot \mathbb{I}_{\{h = n\}}) + j]\right) \leq
	 * \phi(\boldsymbol{x}) \ \ \ \ \ \forall \boldsymbol{x} \in \mathcal{E}_n \f$
	 *
	 * where \f$ \mathbb{I} \f$ is the indicator function, the function CantorLisi::resetStateArray will set the given state space vector
	 * \f$ \boldsymbol{x} \f$ to \f$ [0, \dots, 0, c_l]^T \f$ and return.
	 */
	void resetStateArray(unsigned int* x);

	/*! The structure of the last state in the truncated state space can be illustrated by the following equation.
	 * One can again easily check that
	 *
	 * \f$ \phi(\boldsymbol{x}) \leq \phi(c_u, 0, \dots, 0) = \sum_{h=1}^{n} \left(\frac{1}{h!} \prod_{j=0}^{h-1} [c_u + j]\right)
	 * \ \ \ \ \ \ \ \ \ \ \forall \boldsymbol{x} \in \mathcal{E}_n \f$
	 *
	 * holds and thus the function CantorLisi::setLastStateArray will set the given state space vector  \f$ \boldsymbol{x}  \f$ to
	 * \f$ [c_u, 0, \dots, 0]^T  \f$ and return.
	 */
	void setLastStateArray(unsigned int* x);

	/*! How this function works, is shown in Algorithm 2 in Section 4.4 of the thesis.
	 *
	 */
	void nextState(unsigned int* x);

	/*!	Implements and returns the result of the cantor pairing function
	 *
	 * \f$ \phi(x_1, \dots, x_n) = \sum_{h=1}^{n} \left(\frac{1}{h!} \prod_{j=0}^{h-1} [(\sum_{i=1}^{h} x_i) + j]\right) - \phi(0, \dots, 0, c_l) \f$
	 *
	 * (from "M. Lisi. Some remarks on the Cantor pairing function. Le Matematiche, 62(1):55–65, 2007.") evaluated at the given state
	 * \f$ x \f$.
	 *
	 * Note that this enumeration function sorts the states by the sum \f$ \sum_{i=1}^{n} x_i \f$  of all chemical entities
	 * present in the respective state, in the sense that for any two states \f$ \boldsymbol{x},\boldsymbol{y} \f$
	 * with \f$ \sum_{i=1}^{n} x_i < \sum_{i=1}^{n} y_i \f$ it holds that \f$ \phi(\boldsymbol{x}) < \phi(\boldsymbol{y}) \f$.
	 *
	 * Also note that the subtraction of the constant offset \f$ \phi(0, \dots, 0, c_l) \f$ ensures that the "first state" (i.e.
	 * \f$ \boldsymbol{x}_0 = [0, \dots, 0, c_l] \f$) has index 0.
	 */
	unsigned int phi(unsigned int* x);
};

/*!
 * \brief This class is derived from StateEnum. It was used only for the splitting approach introduced in Chapter 6 of the thesis and is thus not
 *  important for the user. The user is advised to use RectEnum instead, which is a generalization of RectEnum2D to arbitrary numbers of dimensions.
 */
class RectEnum2D : public StateEnum
{
private:
	unsigned int* xLim;
	unsigned int* yLim;

public:
	RectEnum2D(int* xLim, int* yLim);
	virtual ~RectEnum2D();

	void resetStateArray(unsigned int* x);
	void setLastStateArray(unsigned int* x);
	void nextState(unsigned int* x);
	unsigned int phi(unsigned int* x);
};

/*! \brief  This class is derived from StateEnum. It enumerates and truncates an arbitrary dimensional state space by axis-aligned hyperplanes.
 * Thus in two dimensions the truncated state space has the shape of a rectangle.
 */
class RectEnum : public StateEnum
{
private:
	unsigned int* min;
	unsigned int* max;

public:
	/*!
	 *	\param min An array containing a minimum value for each dimension of the truncated state space.
	 *
	 *	\param max An array containing a maximum value for each dimension of the truncated state space.
	 *
	 *	\param dim The dimensionality (i.e the number of species) of the truncated state space.
	 */
	RectEnum(unsigned int* min, unsigned int* max, unsigned int dim);
	virtual ~RectEnum();

	/*!
	 * (Re-)Sets all entries of \f$ \boldsymbol{x} \f$ to the minimum value of the corresponding dimension.
	 */
	void resetStateArray(unsigned int* x);

	/*!
	 * Sets all entries of \f$ \boldsymbol{x} \f$ to the maximum value of the corresponding dimension.
	 */
	void setLastStateArray(unsigned int* x);

	/*!
	 * Increases one of the entries in \f$ \boldsymbol{x} := [x_1, \dots, x_d]^T \f$ by 1. Namely the first entry \f$ x_i \f$ from the back of the array
	 * (i.e. starting with index i=d) for which \f$ x_i \f$ is not equal to the maximum value of the corresponding dimension already.
	 * In addition, all maximum values encountered during that process are set back to the corresponding minimum value.
	 *
	 * Note: If all minimum values were 0 and all maximum values were 9, this function would correspond to regular counting with \f$ d \f$ digits.
	 */
	void nextState(unsigned int* x);

	/*!
	 *	Assigns an unique enumeration value to the given state space vector \f$ \boldsymbol{x} \f$.
	 *
	 *	This is done exploiting the analogy of such truncated state space vectors to digits in a positional numeral system
	 *	(e.g. as explained for the decimal system under RectEnum::nextState).
	 */
	unsigned int phi(unsigned int* x);
};


/*! \brief  This class is derived from StateEnum. It enumerates and truncates an arbitrary dimensional state space by using a mix of
 * dimensions that are truncated "rectangular" (as with RectEnum) and dimensions that are truncated according to a cantor function (as with a CantorLisi).
 */
class MixRectCantor : public StateEnum
{
private:
	RectEnum* rectEnum;
	CantorLisi* cantorEnum;
	unsigned int rectSubDim;
	unsigned int cantorSubDim;
	unsigned int* rectMap; //maps rectSubDim index to full dim index
	unsigned int* cantorMap; //maps cantorSubDim index to full dim index
	unsigned int numRectStates;
	unsigned int* r;
	unsigned int* c;

	void updateMixArray(unsigned int* mixArray, unsigned int* rectArray, unsigned int* cantorArray);
	void updateSubArrays(unsigned int* rectArray, unsigned int* cantorArray, unsigned int* mixArray);

public:
	/*!
	 * \param isRect A boolean array containing the value *true* for every "rectangular" dimension and *false* for every "cantor"
	 * dimension.
	 *
	 * \param min A *dim*-dimensional array containing a minimum value for each "rectangular" dimension of the truncated state space and the lower cut-off
	 *	value \f$ c_l \f$ in the first "cantor" dimension. The values in all other "cantor" dimensions are ignored.
	 *
	 * \param max A *dim*-dimensional array containing a maximum value for each "rectangular" dimension of the truncated state space and the upper cut-off
	 *	value \f$ c_u \f$ in the first "cantor" dimension. The values in all other "cantor" dimensions are ignored.
	 *
	 * \param dim The total dimensionality (i.e the number of species) of the truncated state space.
	 */
	MixRectCantor(unsigned int* min, unsigned int* max, bool* isRect, unsigned int dim);
	virtual ~MixRectCantor();

	/*!
	 * Calls RectEnum::resetStateArray for the "rectangular" dimensions and CantorLisi::resetStateArray for the "cantor" dimensions.
	 */
	void resetStateArray(unsigned int* x);

	/*!
	 * Calls RectEnum::setLastStateArray for the "rectangular" dimensions and CantorLisi::setLastStateArray for the "cantor" dimensions.
	 */
	void setLastStateArray(unsigned int* x);

	/*!
	 * Calls RectEnum::nextState if the last state of the "rectangular" subspace has not been reached yet.
	 *
	 * Otherwise calls CantorLisi::nextState and RectEnum::resetStateArray.
	 *
	 * Note that this orders the states such that a "cantor" substate is paired with every "rectangular" substate, before progressing to
	 * the next "cantor" substate.
	 */
	void nextState(unsigned int* x);

	/*!
	 * Computes the one-dimensional index according to
	 *
	 * \f$ \phi(\boldsymbol{x}) = \phi_{\text{rect}}(\boldsymbol{x}_{\text{rect}}) + |\mathcal{E}_{\text{rect}}| \cdot
	 * \phi_{\text{cant}}(\boldsymbol{x}_{\text{cant}})\f$
	 *
	 * where \f$ \boldsymbol{x}_{\text{rect}} \f$ and \f$ \boldsymbol{x}_{\text{cant}} \f$ correspond to the "rectangular" and "cantor" subspace vector, respectively,
	 * \f$ \phi_{\text{rect}} \f$ and \f$ \phi_{\text{cant}} \f$ correspond to RectEnum::phi and CantorLisi::phi, respectively, and
	 * \f$ \mathcal{E}_{\text{rect}} \f$ is the truncated "rectangular" subspace.
	 */
	unsigned int phi(unsigned int* x);
};
#endif
