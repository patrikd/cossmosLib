#include "StateEnum.h"

// for testing state space enumeration classes
void StateEnum::printStateSpace(int sleepTime)
{
	unsigned int x[dim];

	setLastStateArray(x);
	std::cout << "last state is ";
	printArray(x, dim);

	nextState(x);
	if (isInsideStateSpace(x))
		std::cout << "state after last state is inside state space\n";
	else
		std::cout << "state after last state is not inside state space\n";

	resetStateArray(x);

	if (!hasMatSize)
	{
		computeMatSize();
		hasMatSize = true;
	}
	std::cout << "matSize is " << matSize << std::endl;

	for (unsigned int i=0; i<matSize; i++)
	{
		std::cout << "i=" << i << " and phi=" << phi(x) << " with state vector ";
		printArray(x, dim);

		nextState(x);
		std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
	}
}

void StateEnum::setMatSize(unsigned int matSize)
{
	this->matSize = matSize;
	hasMatSize = true;
}

Epetra_Vector* StateEnum::generateFvec(const std::string& desiredFvec, const Epetra_BlockMap &Map)
{
	unsigned int numSpecies = dim;

	fGen::fGenerator fGen(desiredFvec, numSpecies);
	Epetra_Vector* f = new Epetra_Vector(Map, false);

	unsigned int x[numSpecies];
	resetStateArray(x);
	for (int i=0; i<f->GlobalLength(); i++)
	{
		if (Map.MyGID(i))
		{
			unsigned int localId = Map.LID(i);
			//phiInv(globalId, x);

			(*f)[localId] = fGen.generate(x);
		}

		nextState(x);
	}

	return f;
}

unsigned int StateEnum::getNumStates()
{
	if (!hasMatSize)
	{
		computeMatSize();
		hasMatSize = true;
	}

	return matSize;
}

Epetra_MultiVector* StateEnum::generateG(const char* BFsFilePath, int* maxDegreeVector, bool useBFsFile, const Epetra_BlockMap &Map, bool isMuteOutput)
{
	unsigned int numSpecies = dim;
	fGen::fGenerator** fGens;
	fGen::Monomial** monomial;
	int numMonomials = 0;

	int numFs = 0;
	if (useBFsFile) //either read the basis functions from the given file
	{
		if (!readBFsFile(BFsFilePath, fGens, numFs, numSpecies))
		{
			fprintf(stderr, "%s\n", "could not read desired F");
			exit(EXIT_FAILURE);
		}
	}
	else //or generate all monomial functions (and their "sort of"-inverse) up to a maxDegree
	{
		fillMonomialsArray(maxDegreeVector, monomial, numMonomials, numSpecies);
		numFs = numMonomials;

		if (Map.Comm().MyPID() == 0)
		{
			if (!isMuteOutput) std::cout << "the number of monomials is " << numFs << std::endl;

			std::string* str;
			for (int i=0; i<numFs; i++)
			{
				str = monomial[i]->toString();
				if (!isMuteOutput) std::cout << str->c_str() << " ";
				delete str;
			}
			if (!isMuteOutput) std::cout << std::endl;
		}
	}

	Epetra_MultiVector* G = new Epetra_MultiVector(Map, numFs);
	//countMat = Teuchos::rcp( new Epetra_MultiVector(*Map, numSpecies, false));

	unsigned int x[numSpecies];
	resetStateArray(x);
	for (int i=0; i<G->GlobalLength(); i++)
	{
		if(Map.MyGID(i))
		{
			unsigned int localId = Map.LID(i);
			//phiInv(globalId, x);

			/*
			for (int j=0; j<numSpecies; j++)
			{
				(*(*countMat)(j))[i] = x[j];
			}
			*/

			for (int j=0; j<numFs; j++)
			{
				if (useBFsFile)
					(*(*G)(j))[localId] = fGens[j]->generate(x);
				else
					(*(*G)(j))[localId] = monomial[j]->compute(x);
			}

		}

		nextState(x);
	}

	return G;
}

void StateEnum::computeMatSize()
{
	unsigned int xMax[dim];
	setLastStateArray(xMax);
	matSize = phi(xMax) + 1;
}

CantorLisi::CantorLisi(int* cO, int d)
{
	isFullyConstructed = false;

	dim = d;
	cutOff = new int[2];
	for (int i=0; i<2; i++)
	{
		cutOff[i] = cO[i];
	}

	unsigned int xOffset[dim];
	resetStateArray(xOffset);
	offset = phi(xOffset);

	isFullyConstructed = true;
}

CantorLisi::~CantorLisi()
{
	delete[] cutOff;
}

void CantorLisi::resetStateArray(unsigned int* x)
{
	for (unsigned int i=0; i<dim-1; i++) x[i] = 0;
	x[dim-1] = cutOff[0];
}

void CantorLisi::setLastStateArray(unsigned int* x)
{
	x[0] = cutOff[1];
	for (unsigned int i = 1; i < dim; i++) x[i] = 0;
}

//algorithm I developed on paper
void CantorLisi::nextState(unsigned int* x)
{
	unsigned int z = 0;
	for (unsigned int i=0; i<dim; i++) z += x[i];

	if (z == 0)
	{
		x[dim-1] = 1;
		return;
	}

	//get first entry in x vector that is not zero
	unsigned int ID = 0;
	while (x[ID] == 0) ID++;

	if (ID > 0)
	{
		x[ID]--;
		x[ID-1]++;
		return;
	}

	unsigned int a = x[ID];
	unsigned int b = x[ID+1];
	while (b == 0 && ID+1 < dim)
	{
		x[ID] = b;
		x[ID+1] = a;
		ID++;
		a = x[ID];
		if (ID+1 == dim) break;
		b = x[ID+1];
	}

	x[ID]++;
	if(ID+1 < dim) x[ID+1]--;
}

// the n-degree generalized Cantor pairing function according to Meri Lisi 2007, p.58
unsigned int CantorLisi::phi(unsigned int* x)
{
	unsigned int z[dim];
	unsigned int sum = 0;
	for (unsigned int i=0; i<dim; i++)
	{
		sum += x[i];
		z[i] = sum;
	}

	unsigned int result = 0;
	for (unsigned int h=1; h<=dim; h++)
	{
		unsigned int product = 1;
		for (unsigned int j=0; j<h; j++)
		{
			product *= z[h-1] + j;
		}

		unsigned int factorial = 1;
		for (unsigned int j=1; j<=h; j++)
		{
			factorial *= j;
		}

		result += product / factorial;
	}

	if (isFullyConstructed) result -= offset;

	return result;
}

RectEnum2D::RectEnum2D(int* xL, int* yL)
{
	dim = 2;

	xLim = new unsigned int[2];
	yLim = new unsigned int[2];
	for (int i=0; i<2; i++)
	{
		xLim[i] = xL[i];
		yLim[i] = yL[i];
	}
}

MixRectCantor::MixRectCantor(unsigned int* min, unsigned int* max, bool* isRect, unsigned int dim)
{
	this->dim = dim;
	int* cutOff = new int[2];
	bool firstCantor = true;

	cantorSubDim = 0;
	rectSubDim = 0;
	for (unsigned int i=0; i<dim; i++)
	{
		if (isRect[i])
		{
			rectSubDim++;
		}
		else
		{
			cantorSubDim++;
			if (firstCantor)
			{
				cutOff[0] = min[i];
				cutOff[1] = max[i];
				firstCantor = false;
			}
		}
	}

	cantorMap = new unsigned int[cantorSubDim];
	rectMap = new unsigned int[rectSubDim];
	unsigned int* subMin = new unsigned int[rectSubDim];
	unsigned int* subMax = new unsigned int[rectSubDim];

	unsigned int cantorIdx = 0;
	unsigned int rectIdx = 0;
	for (unsigned int i=0; i<dim; i++)
	{
		if (isRect[i])
		{
			rectMap[rectIdx] = i;
			subMin[rectIdx] = min[i];
			subMax[rectIdx] = max[i];
			rectIdx++;
		}
		else
		{
			cantorMap[cantorIdx] = i;
			cantorIdx++;
		}
	}

	cantorEnum = new CantorLisi(cutOff, cantorSubDim);
	rectEnum = new RectEnum(subMin, subMax, rectSubDim);


	r = new unsigned int[rectSubDim];
	c = new unsigned int[cantorSubDim];
	rectEnum->setLastStateArray(r);
	numRectStates = rectEnum->phi(r) + 1; //because the offset of a RectEnum is always 0

	delete[] subMax;
	delete[] subMin;
	delete[] cutOff;
}

RectEnum::RectEnum(unsigned int* min, unsigned int* max, unsigned int dim)
{
	this->dim = dim;
	this->min = new unsigned int[dim];
	this->max = new unsigned int[dim];

	for (unsigned int i=0; i<dim; i++)
	{
		this->min[i] = min[i];
		this->max[i] = max[i];
	}
}

RectEnum::~RectEnum()
{
	delete[] max;
	delete[] min;
}

void RectEnum::resetStateArray(unsigned int* x)
{
	for (unsigned int i=0; i<dim; i++)
	{
		x[i] = min[i];
	}
}

void RectEnum::setLastStateArray(unsigned int* x)
{
	for (unsigned int i=0; i<dim; i++)
	{
		x[i] = max[i];
	}
}

void RectEnum::nextState(unsigned int* x)
{
	for (int i=dim-1; i>=0; i-- )
	{
		if (i != 0 && x[i]+1 > max[i]) // check i != 0 is used to enlarge the state space automatically
		{
			x[i] = min[i];
		}
		else
		{
			x[i]++;
			break;
		}
	}
}

unsigned int RectEnum::phi(unsigned int* x)
{
	unsigned int index = 0;
	unsigned int multiplier = 1;

	for (int i=dim-1; i>=0; i-- )
	{
		index += multiplier * (x[i] - min[i]);
		multiplier *= (max[i]-min[i]+1);
	}

	return index;
}

//bool RectEnum::isInsideStateSpace(unsigned int* x)
//{
//	//TODO how to handle enlarged state space? is not possible with the current architecture
//	for (unsigned int i=0; i<dim; i++)
//	{
//		if (x[i] < min[i] || x[i] > max[i]) return false;
//	}
//
//	return true;
//}

RectEnum2D::~RectEnum2D()
{
	delete[] xLim;
	delete[] yLim;
}

MixRectCantor::~MixRectCantor()
{
	delete[] r;
	delete[] c;
	delete rectEnum;
	delete cantorEnum;
	delete[] rectMap;
	delete[] cantorMap;
}

void MixRectCantor::updateMixArray(unsigned int* x, unsigned int* r, unsigned int* c)
{
	for (unsigned int i = 0; i < rectSubDim; i++)
	{
		x[rectMap[i]] = r[i];
	}

	for (unsigned int i = 0; i < cantorSubDim; i++)
	{
		x[cantorMap[i]] = c[i];
	}
}

void MixRectCantor::resetStateArray(unsigned int* x)
{
	rectEnum->resetStateArray(r);
	cantorEnum->resetStateArray(c);

	updateMixArray(x, r, c);
}

void MixRectCantor::setLastStateArray(unsigned int* x)
{
	rectEnum->setLastStateArray(r);
	cantorEnum->setLastStateArray(c);

	updateMixArray(x, r, c);
}

void MixRectCantor::updateSubArrays(unsigned int* r, unsigned int* c, unsigned int* x)
{
	for (unsigned int i = 0; i < rectSubDim; i++)
	{
		r[i] = x[rectMap[i]];
	}

	for (unsigned int j=0; j<cantorSubDim; j++)
	{
		c[j] = x[cantorMap[j]];
	}
}

void MixRectCantor::nextState(unsigned int* x)
{
	updateSubArrays(r, c, x);

	unsigned int curRectPhi = rectEnum->phi(r);
	if (curRectPhi+1 == numRectStates)
	{
		rectEnum->resetStateArray(r);
		cantorEnum->nextState(c);
	}
	else
	{
		rectEnum->nextState(r);
	}

	updateMixArray(x, r, c);
}

unsigned int MixRectCantor::phi(unsigned int* x)
{
	updateSubArrays(r, c, x);

	return rectEnum->phi(r) + numRectStates * cantorEnum->phi(c);
}

void RectEnum2D::resetStateArray(unsigned int* x)
{
	x[0] = xLim[0];
	x[1] = yLim[0];
}

void RectEnum2D::setLastStateArray(unsigned int* x)
{
	x[0] = xLim[1];
	x[1] = yLim[1];
}

void RectEnum2D::nextState(unsigned int* x)
{
	if (x[0] < xLim[1])
	{
		x[0] += 1;
	}
	else
	{
		x[0] = xLim[0];
		x[1] += 1;
	}
}

unsigned int RectEnum2D::phi(unsigned int* x)
{
	return (x[1] - yLim[0]) * (xLim[1] - xLim[0] + 1) + x[0] - xLim[0];
}

bool StateEnum::isInsideStateSpace(unsigned int* x)
{
	int yID = phi(x);

	if (!hasMatSize)
	{
		computeMatSize();
		hasMatSize = true;
	}

	return (yID < ((int) matSize) && yID >= 0);
}

StateEnum::StateEnum()
{
	dim = 0;
	matSize = 0;
	hasMatSize = false;
}

//bool RectEnum2D::isInsideStateSpace(unsigned int* x)
//{
//	if (x[0] >= xLim[0] && x[0] <= xLim[1] && x[1] >= yLim[0] && x[1] <= yLim[1]) return true;
//	//printArray(x, 2);
//	return false;
//}

int StateEnum::getDim() const {
	return dim;
}
