/*
 * CossmosParameters.cpp
 *
 *  Created on: May 24, 2018
 *      Author: patrik
 */

#include "cossmosSetups.h"

cossmosSetups::cossmosSetups(const char* expectFncts, const char* desiredFfncts, unsigned int numSpecies)
{
	netName = "myNet";
	computePi = true;
	numRestarts = 200;
	numBlocks = 3;
	blockSize = 1;
	convergeTol = 0.00000001;
	storePi = false;
	setVerboseSolvers = false;
	useBFsFile = false;
	useResultsFile = false;
	muteOutput = false;
	this->numSpecies = numSpecies;
	maxDegreeVector = new int[numSpecies];
	for (unsigned int i=0; i<numSpecies; i++) maxDegreeVector[i] = 1;
	maxIterLS = 10000;
	BFsFilePath = "";
	resultsFilePath = "";
	computeOutflowRate = true;

	this->desiredFfncts = desiredFfncts;
	this->expectFncts = expectFncts;
//	printStateEnum = false;
//	sleepTime = 100;
//	doResetParams = false;
//	numSpecies = numSpec;
}

cossmosSetups::~cossmosSetups()
{
//	if (doResetParams)
//	{
//		delete[] resetParams;
//	}
	// TODO Auto-generated destructor stub
}
