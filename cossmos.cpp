#include "cossmos.h"

bool cossmosAlgo::printTimingInfo(clock_t begin, clock_t end, double &total_elapsed_secs, const std::string info, int PID)
{
	//only print timing info for the first process
	if (PID != 0) return true;

	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	total_elapsed_secs += elapsed_secs;
	//std::cout << info << "\telapsed seconds is: " << elapsed_secs << std::endl;

	return true;
}

Epetra_Vector* cossmosAlgo::getLScoefs(double **AArr, double* bArr, int n, Epetra_Map& smallMap,
							double& total_elapsed_secs, const Epetra_Comm& Comm, int maxIter, bool setVerboseSolvers)
{
	Epetra_Vector* c = new Epetra_Vector(smallMap);

	//Epetra_MultiVector A_MV(Copy, smallMap, AArr, n, n);
	Epetra_Vector b(Copy, smallMap, bArr);

	clock_t begin = clock();
	// construct the matrix A from the corresponding multivector
	Epetra_CrsMatrix A = Epetra_CrsMatrix(Copy, smallMap, n);
	fillMatByArr(A, AArr, n);
	//A.Print(cout);
	printTimingInfo(begin, clock(), total_elapsed_secs, "construct A", Comm.MyPID());
	//EpetraExt::RowMatrixToMatlabFile("./A.dat", A);


	begin = clock();
	// define and solve linear problem in order to get the C vector

	Epetra_LinearProblem smallProblem(&A, c, &b);
	AztecOO smallSolver(smallProblem);
	if (!setVerboseSolvers) smallSolver.SetAztecOption(AZ_output, AZ_none);
	smallSolver.SetAztecOption(AZ_conv, AZ_noscaled);
	smallSolver.Iterate(maxIter, 1e-15);
	printTimingInfo(begin, clock(), total_elapsed_secs, "solve linear problem", Comm.MyPID());

	return c;
}

Epetra_Vector* cossmosAlgo::getWeightedErrorCoefs(Epetra_Vector* f, Epetra_MultiVector& F, Epetra_Vector* pi, clock_t begin, int n,
													const Epetra_BlockMap& map, Epetra_Map& smallMap, double& total_elapsed_secs, const Epetra_Comm& Comm)
{
	int maxIterLS = setups->getMaxIterLs();
	bool setVerboseSolvers = setups->isSetVerboseSolvers();
	Epetra_Map Map(map.NumGlobalElements(), map.IndexBase(), map.Comm());


	begin = clock();
	// construct the omega^-1 matrix for the generalized least squares problem
	// omega^-1 is the sparse (dxd)-matrix with the entries of pi on the diagonal
	Epetra_CrsMatrix omega = Epetra_CrsMatrix(Copy, Map, 1, true);
	fillMatDiagByVec(omega, pi, Map, Comm);
	printTimingInfo(begin, clock(), total_elapsed_secs, "construct omega^-1", Comm.MyPID());
	//if (writeResults) EpetraExt::RowMatrixToMatlabFile("./omega.dat", *omega);


	// put all n elements (of futur vector b) on all processors
	//int myElements = 0;
	//if (Comm.MyPID() == 0) myElements = n;

	begin = clock();
	// compute the n-dimensional vector b = G' * omega^-1 * g
	// note: b_temp = omega^-1 * g
	Epetra_Vector b_temp = Epetra_Vector(Map);
	omega.Multiply(false, *f, b_temp);
	//Epetra_Vector b = Epetra_Vector(smallMap);
	//b.Multiply('T', 'N', 1, G, b_temp, 0);

	double b[n];
	for (int i=0; i<n; i++)
		F(i)->Dot(b_temp, &b[i]);

	printTimingInfo(begin, clock(), total_elapsed_secs, "compute b", Comm.MyPID());

	//printArray(b, n);
	//EpetraExt::MultiVectorToMatlabFile("./b.dat", b);


	begin = clock();
	// compute the (nxn)-dimensional multivector A_MV = G' * omega^-1 * G
	// note: A_temp = omega^-1 * G
	Epetra_MultiVector A_temp = Epetra_MultiVector(Map, n);
	omega.Multiply(false, F, A_temp);
	Epetra_Map replicateMap(n, n, 0, Comm);
	Epetra_MultiVector A_MV = Epetra_MultiVector(replicateMap, n);
	A_MV.Multiply('T', 'N', 1, F, A_temp, 0);
	double** A = new double*[n];
	if (Comm.MyPID() == 0)
	{
		for (int i=0; i<n; i++)
		{
			A[i] = new double[n];
		}

		for (int i=0; i<n; i++)
		{
			for (int j=i; j<n; j++)
			{
				double tmp = A_MV[i][j];
				A[i][j] = tmp; // A is symmetric
				A[j][i] = tmp; // A is symmetric
			}

	//		for (int j=0; j<n; j++)
	//		{
	//			G(i)->Dot(*A_temp(j), &A[i][j]);
	//		}
			//A_MV(i)->ExtractCopy(A[i]);
			//Comm.Broadcast(A[i], n, 0);

			//cout << Comm.MyPID() << ": " << i << endl;
			//printArray(A[i], n); //exit(EXIT_FAILURE);
		}
	}

	printTimingInfo(begin, clock(), total_elapsed_secs, "compute A_MV", Comm.MyPID());

/*
	for (int i=0; i<n; i++)
		printArray(A[i], n);
*/

	//EpetraExt::MultiVectorToMatlabFile("./A_MV.dat", A_MV);

	Epetra_Vector* c;

	//TODO make that modular (with an Interface e.g. coefsComputer)
//	switch (meth)
//	{
//
//	case CoefMethod::LS:
		//std::cout << "used LS coefMethod\n";
	c = getLScoefs(&A[0], &b[0], n, smallMap, total_elapsed_secs, Comm, maxIterLS, setVerboseSolvers);
/*	break;

	case CoefMethod::LASSO:
		std::cout << "used LASSO coefMethod\n";
		return getLASSOcoefs(A_MV, bVec, n, smallMap, total_elapsed_secs, Comm, regParam);

	case CoefMethod::ridge:
		std::cout << "used ridge coefMethod\n";
		return getRidgeCoefs(A_MV, bVec, n, smallMap, total_elapsed_secs, Comm, regParam);

	default:
		fprintf(stderr, "%s\n", "ERROR: the requested coefficient method for the basis function method is unknown!");
		exit(EXIT_FAILURE);
	}
*/

	if (Comm.MyPID() == 0) for (int i=0; i<n; i++) delete[] A[i];
	delete[] A;

	return c;
}

bool cossmosAlgo::scaleWRTpi(Epetra_Vector* f, Epetra_Vector* pi, int NumMyElements, const Epetra_Comm& Comm, double &scaleFact)
{
	double mySum = 0;
	for (int i=0; i<f->MyLength(); i++)
		mySum += std::abs((*f)[i]) * (*pi)[i];
	scaleFact = 0;
	Comm.SumAll(&mySum, &scaleFact, 1);
	f->Scale(1/scaleFact);

	return true;
}

bool cossmosAlgo::scaleWRTmaxAbs(Epetra_Vector* f, double &scaleFact)
{
	double max, min;
	f->MaxValue(&max);
	f->MinValue(&min);

	scaleFact = std::max<double>(max, -min);
	f->Scale(1/scaleFact);

	return true;
}

bool cossmosAlgo::makeOrthogonalToPi(Epetra_Vector* f, Epetra_Vector* pi, Epetra_Vector* h, int NumMyElements)
{
	//the right-hand-side vector needs to be orthogonal to the solution vector pi of the previous problem
	//=> construct it that way

	double *fVals;
	f->ExtractView(&fVals);
	for (int i = 0; i < NumMyElements; ++i)
		fVals[i] *= (*h)[i];

	//compute the dot product of f and pi
	double result[1] = { 0 };
	f->Dot(*pi, result);

	//if (writeResults) EpetraExt::MultiVectorToMatlabFile("./grand.dat", *g);
	//compute the dot product of h and pi
	double result2[1] = { 0 };
	h->Dot(*pi, result2);

	//subtract the dot product from each entry in f
	double scaleFact = result[0] / result2[0];
	double* f_values;
	f->ExtractView(&f_values);
	for (int i = 0; i < NumMyElements; ++i)
		f_values[i] -= scaleFact * (*h)[i];

	return true;
}

double cossmosAlgo::dotWRTpi(Epetra_Vector &v1, Epetra_Vector &v2, TVector pi, Epetra_Comm &Comm)
{
	double mySum = 0;
	for (int i=0; i<v1.MyLength(); i++)
		mySum += v1[i] * v2[i] * (*pi)[i];

	double dotProd = 0;
	Comm.SumAll(&mySum, &dotProd, 1);

	return dotProd;
}


bool cossmosAlgo::backTransform2(Epetra_Vector* c, double* scaleFact)
{
	int n = c->MyLength();

	for (int i=0; i<n; i++)
	{
		(*c)[i] *= 1/scaleFact[c->Map().GID(i)];
	}

	return true;
}

void cossmosAlgo::printApproxError(const Epetra_BlockMap& Map, Epetra_CrsMatrix* Qt, Epetra_Vector* g, Epetra_Vector* f, Epetra_Vector& pi, Epetra_Vector* h,
						int NumMyElements)
{
	double approxError = 0;
	Epetra_Vector Qg = Epetra_Vector(Map);
	Epetra_Vector piCpy = Epetra_Vector(pi);

	Qt->Multiply(true, *g, Qg);
	Qg.Update(1, *f, 1);

	double mySum = 0;
	for (int i=0; i<Qg.MyLength(); i++)
		mySum += std::abs(Qg[i] * pi[i]);
	Map.Comm().SumAll(&mySum, &approxError, 1);

	//transform pi
	double* piCpyVals;
	piCpy.ExtractView(&piCpyVals);
	for (int i=0; i<piCpy.MyLength(); i++)
	{
		piCpyVals[i] *= (*h)[i];
	}

	//normalize pi
	mySum = 0;
	for (int i=0; i<piCpy.MyLength(); i++)
		mySum += piCpy[i];
	double scaleFact = 0;
	Map.Comm().SumAll(&mySum, &scaleFact, 1);

	approxError *= std::abs(1.0/scaleFact);
	if (Map.Comm().MyPID() == 0 && !setups->isMuteOutput())
		std::cout << "approximation error for the basis function method is: " <<  approxError << std::endl;
}

Epetra_Vector* cossmosAlgo::basisFunctionMethod(const std::string& desiredFvec, StateEnum* stateEnum, Epetra_Vector* h, Epetra_Vector* pi,
												Epetra_CrsMatrix* Qt)
{
	// Generate the matrix G that holds all basis function vectors
	Epetra_MultiVector* G = stateEnum->generateG(setups->getBFsFilePath(), setups->getMaxDegree(), setups->isUseBFsFile(), Qt->Map(), setups->isMuteOutput());

	// Define some useful variables
	double total_elapsed_secs = 0;
	int n = G->NumVectors();
	int NumMyElements = Qt->Map().NumMyElements();

	// Generate the vector that represents the r.h.s. of the Poisson equation.
	Epetra_Vector* f = stateEnum->generateFvec(desiredFvec, Qt->Map());

	// This r.h.s vector needs to be orthogonal to the stationary distribution vector pi.
	// To ensure that, we call the following function:
	makeOrthogonalToPi(f, pi, h, NumMyElements);

	// Now the dot product of f and pi should be zero.
	/* Uncomment to check that
	double result[1] = {0};
	f->Dot(*pi, result);
	if (std::abs(result[0]) > 1e-8 && !setups->isMuteOutput())
		std::cout << "WARNING: dot product <pi,g> " << result[0] << " might be a bit too large!" << std::endl;
	*/

	// Compute the matrix F = [ f_1, f_2, ..., f_n ] = Q * [ g_1, g_2, ..., g_n]
	// It contains the image of the basis function vectors under the generator matrix Q
	clock_t begin = clock();
	Epetra_MultiVector F = Epetra_MultiVector(Qt->Map(), n);
	bool transQt = true;
	Qt->Multiply(transQt, *G, F);
	printTimingInfo(begin, clock(), total_elapsed_secs, "compute F", Qt->Map().Comm().MyPID());

	// Perform two scalings on each vector f_i.
	// Those are required because pi and Q are scaled as well.
	double* scaleFact = new double[n];
	for (int i=0; i<n; i++)
	{
		scaleWRTpi(F(i), pi, NumMyElements, Qt->Map().Comm(), scaleFact[i]);
	}

	double* scaleFact2 = new double[n];
	for (int i=0; i<n; i++)
	{
		scaleWRTmaxAbs(F(i), scaleFact2[i]);
	}

	// We want to solve the low-dimensional least squares problem on only a single processor (i.e. processor 0).
	// This is set-up here: The number of elements assigned to the processor for the "small" least squares problem is set to 0 for all but processor 0.
	int numMySmallElems = 0;
	if (Qt->Map().Comm().MyPID() == 0) numMySmallElems = n;
	Epetra_Map smallMap(n,numMySmallElems, 0, Qt->Map().Comm());

	// Solve the low-dimensional least squares problem
	Epetra_Vector* c = getWeightedErrorCoefs(f, F, pi, begin, n, Qt->Map(), smallMap, total_elapsed_secs, Qt->Map().Comm());

	// Since some scalings were performed on the f_i's, we need to "unscale" the optimal solution of the least squares problem.
	backTransform2(c, scaleFact2);
	backTransform2(c, scaleFact);
	c->Scale(-1);

	// Broadcast the optimal solution of the least squares problem to all processors (since it has only been computed on processor 0).
	double cBroad[n];
	if(Qt->Map().Comm().MyPID() == 0) c->ExtractCopy(cBroad);
	Qt->Map().Comm().Broadcast(cBroad, n, 0);

	// Construct the Poisson equation solution vector g using the least squares problem solution.
	// This is done by computing g as the weighted sum of the basis functions (stored in matrix G).
	begin = clock();
	Epetra_Vector* g = new Epetra_Vector(Qt->Map()); // Note: This constructor sets all vector entries to zero.
	for (int i=0; i<n; i++)
	{
		g->Update(cBroad[i] , *((*G)(i)), 1); //obfuscated way of adding the scaled vector c_i * g_i to solution vector g
	}
	printTimingInfo(begin, clock(), total_elapsed_secs, "compute f", Qt->Map().Comm().MyPID());
	if (Qt->Map().Comm().MyPID() == 0 && !setups->isMuteOutput()) std::cout << "total elapsed seconds in BFM is: " << total_elapsed_secs << std::endl;

	// Print the BFM approximation error
	printApproxError(Qt->Map(), Qt, g, f, *pi, h, NumMyElements);

	// Free allocated memory
	delete[] scaleFact;
	delete[] scaleFact2;
	delete c; delete G; delete f;

	return g;
}


double cossmosAlgo::innerProduct(double* a, double* b, int length)
{
	double iP = 0;
	for (int i=0; i<length; i++)
	{
		iP += a[i]*b[i];
	}
	return iP;
}

// this method is based on an Trilinos example file
// https://trilinos.org/docs/r12.12/packages/anasazi/doc/html/BlockKrylovSchur_2BlockKrylovSchurEpetraExGenAmesos_8cpp-example.html
Epetra_Vector* cossmosAlgo::computeStationaryDist(Epetra_CrsMatrix* Qt)
{
	// Create a linear problem that holds the fixed-point equation Qt*pi=0.
	// Note: To find a non trivial solution to this equation one can interpret it as an eigenvalue problem.
	Epetra_LinearProblem AmesosProblem;
	AmesosProblem.SetOperator(Qt);
	Amesos amesosFactory;
	RCP<Amesos_BaseSolver> AmesosSolver;

	// Define a list of possible solvers that solve the problem
	const int numSolverNames = 8;
	const char* solverNames[8] = { "Klu", "Mumps", "Paradiso", "Taucs", "CSparse", "Umfpack", "Superlu", "Superludist"};

	// Use the solver which is at the same:
	// (i) time installed on the platform the program is run on and
	// (ii) as far back in the list as possible
	// This should be "Superludist", when one wants to solve large scale problems.
	for (int k = 0; k < numSolverNames; ++k)
	{
		const char* const solverName = solverNames[k];
		if (amesosFactory.Query(solverName))
		{
			AmesosSolver = rcp(amesosFactory.Create(solverName, AmesosProblem));
			if (Qt->Map().Comm().MyPID() == 0)
			{
				if (!setups->isMuteOutput()) std::cout << "Amesos solver: \"" << solverName << "\""<< std::endl;
			}
		}
	}

	// If none of the listed solvers is installed abort the program
	if (AmesosSolver.is_null())
	{
		fprintf(stderr, "%s\n", "Amesos appears not to have any solvers enabled.");
		exit(EXIT_FAILURE);
	}

	// Start a timer
	clock_t begin = clock();

	// Factorize the generator matrix Q.
	// Note: This is by far the most time consuming step in the whole cossmos algorithm!
	AmesosSolver->SymbolicFactorization();
	AmesosSolver->NumericFactorization();


	// Create a parameter list specifying the solver settings to pass to the solver manager
	Teuchos::ParameterList MyPL;
	int verbosity;
	if (setups->isSetVerboseSolvers())
		verbosity = Anasazi::Errors + Anasazi::Warnings + Anasazi::FinalSummary;
	else
		verbosity = Anasazi::Errors;
	MyPL.set("Verbosity", verbosity);
	MyPL.set("Which", "LM"); //Note: We actually solve the inverse problem! That's why we look for the largest (instead of the smallest) magnitude eigenvalue.
	MyPL.set("Block Size", setups->getBlockSize());
	MyPL.set("Num Blocks", setups->getNumBlocks());
	MyPL.set("Maximum Restarts", setups->getNumRestarts());
	MyPL.set("Convergence Tolerance", setups->getConvergeTol());

	// Initialize the solution vector to the uniform distribution over the truncated state space
	TMultiVector ivec = Teuchos::rcp(new Epetra_MultiVector(Qt->Map(), 1));
	double uniDistConst = 1.0 / ((double) (ivec->GlobalLength()));
	ivec->PutScalar(uniDistConst);

	// This line converts the generator matrix into an Amesos operator.
	// The Amesos operator essentially also represents the generator matrix, but is in a format, that the Anasazi eigenproblem solver can work with.
	Teuchos::RCP<AmesosGenOp> Aop = Teuchos::rcp(new AmesosGenOp(AmesosSolver));

	// Create the eigenproblem.
	Teuchos::RCP<Anasazi::BasicEigenproblem<double, MV, OP> > MyProblem =
			Teuchos::rcp(new Anasazi::BasicEigenproblem<double, MV, OP>(Aop, ivec));

	// Set the number of eigenvalues requested
	MyProblem->setNEV(1);
	MyProblem->setHermitian(false);

	// Inform the eigenproblem that you are finished passing information to it
	if (!MyProblem->setProblem())
		if (Qt->Map().Comm().MyPID() == 0 && !setups->isMuteOutput())
			std::cout << "Anasazi::BasicEigenproblem::setProblem() returned with error."<< std::endl;

	// Initialize the Block Arnoldi solver that will solve the eigenproblem
	Anasazi::BlockKrylovSchurSolMgr<double, MV, OP> MySolverMgr(MyProblem, MyPL);

	// Solve the eigenproblem to the specified tolerances
	Anasazi::ReturnType returnCode = MySolverMgr.solve();

	// If no solution has been found, abort the program
	if (returnCode != Anasazi::Converged)
	{
		if (Qt->Map().Comm().MyPID() == 0 && !setups->isMuteOutput())
			std::cout << "Anasazi::EigensolverMgr::solve() returned unconverged." << std::endl;

		MPI_Finalize();
	}

	// Read the eigenvalues and eigenvectors from the eigenproblem solution.
	Anasazi::Eigensolution<double, MV> sol = MyProblem->getSolution();
	std::vector<Anasazi::Value<double> > evals = sol.Evals;
	TMultiVector evecs = sol.Evecs;

	int numev = sol.numVecs;
	if (numev > 0) //If a solution to the eigenvalue problem has been found
	{
		// Reconstruct the eigenvalues.  The ones that Anasazi gave back are the inverse of the original eigenvalues.
		// Reconstruct the eigenvectors too.
		Teuchos::SerialDenseMatrix<int, double> dmatr(numev, numev);
		MV tempvec(Qt->Map(), numev);
		Qt->Apply(*evecs, tempvec);
		MVT::MvTransMv(1.0, tempvec, *evecs, dmatr);

		// Print how much time has been consumed, to estimate the stationary distribution (if the output is not muted).
		if (Qt->Map().Comm().MyPID() == 0)
		{
			//stop timer
			clock_t end = clock();
			double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
			if (!setups->isMuteOutput()) std::cout << "elapsed seconds to compute stationary distribution: " << elapsed_secs << std::endl;
		}

		// Also print the smallest eigenvalue (if the verbose solver option was chosen by the user).
		if (!setups->isMuteOutput() && setups->isSetVerboseSolvers() && Qt->Map().Comm().MyPID() == 0)
		{
			double compeval = 0.0;
			std::cout.setf(std::ios_base::right, std::ios_base::adjustfield);
			std::cout << "Actual Eigenvalues (obtained by Rayleigh quotient) : " << std::endl;
			std::cout << "------------------------------------------------------" << std::endl;
			std::cout << std::setw(16) << "Real Part" << std::setw(16) << "Rayleigh Error" << std::endl;
			std::cout << "------------------------------------------------------" << std::endl;
			for (int i = 0; i < numev; ++i)
			{
				compeval = dmatr(i, i);
				std::cout << std::setw(16) << compeval << std::setw(16) << std::fabs(compeval - 1.0 / evals[i].realpart) << std::endl;
			}
			std::cout << "------------------------------------------------------" << std::endl;
		}

		// The estimate for the stationary distribution is the computed eigenvector.
		Epetra_Vector* pi = new Epetra_Vector(*(*sol.Evecs)(0));

		// Stores the estimated stationary distribution to a file (if the user wishes to).
		if (setups->isStorePi())
		{
			std::stringstream ss;
			ss << "./pi_" << setups->getNetName().c_str() << ".txt";
			EpetraExt::MultiVectorToMatrixMarketFile(ss.str().c_str(), *pi);
		}

		return pi;
	}

	// This code is only reached if no solution to the eigenvalue problem has been found
	fprintf(stderr, "%s\n", "could not compute the stationary distribution");
	exit(EXIT_FAILURE);
}


void cossmosAlgo::computeSensitivities(const Epetra_Comm& Comm, Epetra_Vector* g, AugmentedModel &augModel, double* gGathered, double* piGathered,
							std::ofstream& resultsFile, double* sensitivity)
{
	double* hVals = NULL;
	if (Comm.MyPID() == 0)
	{
		hVals = new double[g->GlobalLength()];
		int S = augModel.getNumSpecies();
		unsigned int* x;
		x = new unsigned int[S];

		unsigned int* y;
		y = new unsigned int[S];
		//double sensitivity[augModel.getNumParameters()];

		for (int omega = 0; omega < augModel.getNumParameters(); omega++)
		{
			sensitivity[omega] = 0;
			for (int k = 0; k < augModel.getNumReactions(); k++)
			{
				//reset state vector to state with ID 0
				augModel.resetStateArray(x);

				for (int i = 0; i < g->GlobalLength(); i++)
				{
					//augModel.phiInv(i, x);

					double lamKderOmI = augModel.lamdaKderivOmegaI(k, omega, x);
					if (lamKderOmI != 0)
					{
						for (int j = 0; j < S; j++)
						{
							//Warning:
							//I am assigning a potentially negative number (assume x[j]==0 and the entry of the stoichiometric vector is negative)
							//to an unsigned integer here. This means y[j] can potentially become a very large integer. But because x[j]==0 usually
							//implies lamKderOmI == 0 (assuming the user chose meaningful reaction kinetics) this event will never occur.
							y[j] = x[j] + augModel.getStoichMatAt(k, j);
						}

						int newI = augModel.getLinearIndex(y);

						if (newI >= g->GlobalLength() || newI < 0)
							newI = augModel.getDesignatedState();

						hVals[i] = gGathered[newI] - gGathered[i];
						hVals[i] *= lamKderOmI;
					}
					else
					{
						hVals[i] = 0;
					}

					augModel.nextState(x);
				}

				//double result[1] = {0};
				//int suc = pi->Dot(Epetra_Vector(View, Map, hVals), result);
				//if (omega==0 && k == 0) std::cout << sumAllEntries(piGathered, f->GlobalLength()) << std::endl;
				//std::stringstream ss;
				//ss   << "./hVals" << omega << k << ".dat";
				//writeArrayToMatlabFile(ss.str().c_str(), hVals, f->GlobalLength());
				double result = innerProduct(piGathered, hVals, g->GlobalLength());
				sensitivity[omega] += result;
			}

			if (Comm.MyPID() == 0)
			{
				std::stringstream ss; ss << std::setprecision(20) << "sensitivity for parameter " << omega << " is: " << sensitivity[omega] << std::endl;
				if (!setups->isMuteOutput()) std::cout << ss.str();
				if (setups->isUseResultsFile()) resultsFile << ss.str();
			}
		}

		delete[] x; delete[] y; delete[] hVals;
	}
}

cossmosResults* cossmosAlgo::cossmos(AugmentedModel &augModel, Epetra_Comm &Comm)
{
	if (Comm.MyPID() == 0 && !setups->isMuteOutput()) std::cout << "computing sensitivities for " << setups->getNetName() << " network\n";

	Epetra_Vector* invPropSum;
	Epetra_CrsMatrix* Qt;
	Epetra_Map* Map;

	// Create the generator matrix Q
	clock_t begin = clock();
	augModel.createQ(Qt, invPropSum, Map, Comm);
	double dummyTot;
	printTimingInfo(begin, clock(), dummyTot, "generating matrix Q", Comm.MyPID());

	// Print information on how the rows of Q (i.e. the elements) are distributed over the different processors
	int NumMyElements = Map->NumMyElements();
	if (!setups->isMuteOutput())
		std::cout << "Process " << Comm.MyPID() << " has " << NumMyElements << " of " << Map->NumGlobalElements() << " elements." << std::endl;

	Epetra_Vector* pi;
	if (setups->isComputePi()) // If the user wants to compute the stationary distribution with this cossmos call
	{
		// Estimate the stationary distribution of the CTMC model with the given generator
		pi = computeStationaryDist(Qt);
	}
	else // If the user wants to load the stationary distribution from a previous cossmos call
	{
		// Load the estimated stationary distribution from a text file named pi_<modelName>.txt located in the directory the program was called in
		Epetra_MultiVector* mv = NULL;
		std::stringstream ss;
		std::string str = setups->getNetName();
		ss  << "./pi_" << str << ".txt";
		int result = EpetraExt::MatrixMarketFileToMultiVector(ss.str().c_str(), *Map, mv);
		if (result != 0)
		{
			fprintf(stderr, "%s\n", "could not read from pi.txt properly");
			exit(EXIT_FAILURE);
		}
		pi = new Epetra_Vector( *((*mv)(0)) );
	}

	// Create and open results file (only if the user wishes to)
	std::ofstream resultsFile(setups->getResultsFilePath());
	if (Comm.MyPID() == 0)
	{
		if (setups->isUseResultsFile() && !resultsFile.is_open())
		{
			fprintf(stderr, "%s\n", "could not open provided results file");
			exit(EXIT_FAILURE);
		}
	}

	// Compute and print the outflow rate of the estimated stationary distribution (only if the user wishes to)
	if(setups->isComputeOutflowRate())
	{
		double outflowRate = augModel.computeOutflowRate(pi, invPropSum);

		if (Comm.MyPID() == 0)
		{
			std::stringstream ss; ss << "Outflow rate: " << outflowRate << std::endl;
			if (!setups->isMuteOutput()) std::cout << ss.str();
			if (setups->isUseResultsFile()) resultsFile << ss.str();
		}
	}

	// Split the strings that specify the expectations and output functions that should be computed
	std::vector<std::string> fs = split(setups->getExpectFncts(), ',');
	std::vector<std::string> desiredFvec = split(setups->getDesiredFfncts(), ',');

	// If the CossmosResults object associated to this instance of the cossmosAlgo object has been filled before
	// (e.g. with a previous call to cossmosAlgo::cossmos()),
	// then delete the object and...
	if (hasResults) delete res;

	// Constuct the cossmosResult object. It will hold the results of this call, after the call has been successfully executed.
	res = new cossmosResults(fs.size(), desiredFvec.size(), augModel.getNumParameters());
	hasResults = true;

	// Compute the user defined expectations (and print them)
	augModel.computeExpectations(fs, pi, resultsFile, invPropSum, res, setups);

	double* piGathered;
	for (unsigned int i=0; i<desiredFvec.size(); i++) // For every user defined output function
	{
		// Print the output function (if the user wishes to)
		if (Comm.MyPID() == 0)
		{
			std::stringstream ss; ss << "\nComputing sensitivities for g=" << desiredFvec[i] << std::endl;
			if (!setups->isMuteOutput()) std::cout << ss.str();
			if (setups->isUseResultsFile()) resultsFile << ss.str();
		}

		bool isFirstIter = (i == 0);
		bool isLastIter = (i == desiredFvec.size()-1);

		// Execute the basis function method (BFM)
		Epetra_Vector* g = basisFunctionMethod(desiredFvec[i], augModel.getStateEnum(), invPropSum, pi, Qt);

		// Transform pi
		double* piVals;
		pi->ExtractView(&piVals);
		for (int j=0; j<pi->MyLength(); j++)
		{
			piVals[j] *= (*invPropSum)[j];
		}

		// Normalize pi
		double mySum = 0;
		for (int j=0; j<pi->MyLength(); j++)
			mySum += (*pi)[j];
		double scaleFact = 0;
		Comm.SumAll(&mySum, &scaleFact, 1);
		pi->Scale(1/scaleFact);


		double* gVals;
		g->ExtractView(&gVals);
		double* gGathered;
		if (Comm.NumProc() > 1){ // If more than one processor is used
			// We need to gather the distributed vectors for the stationary distribution an the Poisson equation solution
			//on a single processor (i.e. processor 0). This is because we perform the actual sensitivity computation on a
			//single processor only. Thus we allocate memory for the gathered vectors on processor 0.
			if (Comm.MyPID() == 0)
			{
				gGathered = new double[g->GlobalLength()];
				if (isFirstIter) piGathered = new double[g->GlobalLength()]; // Note: piGathered remains the same whatever the output function is
			}
			else // On all other processors we don't need the gathered vectors and therefore we don't allocate memory for them.
			{
				gGathered = NULL;
				if (isFirstIter) piGathered = NULL; // Note: piGathered remains the same whatever the output function is
			}

			// Perform the gathering of the distributed vectors to processor 0
			Epetra_MpiComm& mpiComm = dynamic_cast<Epetra_MpiComm&>(Comm);
			MPI_Gather(gVals, g->MyLength(), MPI_DOUBLE, gGathered, g->MyLength(), MPI_DOUBLE, 0, mpiComm.GetMpiComm());
			if (isFirstIter) MPI_Gather(piVals, g->MyLength(), MPI_DOUBLE, piGathered, g->MyLength(), MPI_DOUBLE, 0, mpiComm.GetMpiComm());

			// And finally compute the sensitivities (only on processor 0)
			computeSensitivities(Comm, g, augModel, gGathered, piGathered, resultsFile, res->getSensVals()[i]);

			// Free the allocated memory
			if (Comm.MyPID() == 0)
			{
				delete[] gGathered;
				if (isLastIter) delete[] piGathered;
			}
		}
		else // If only one processor is used
		{
			// We can directly call computeSensitivities without gathering the vectors
			computeSensitivities(Comm, g, augModel, gVals, piVals, resultsFile, res->getSensVals()[i]);
		}

		if (!isLastIter) // After the sensitivities for the last output function have been computed...
		{
			// ...unnormalize pi and...
			pi->Scale(scaleFact);

			// ...unscale pi (that way it can be reused for further calls)
			double* piVals;
			pi->ExtractView(&piVals);
			for (int i=0; i<pi->MyLength(); i++)
			{
				piVals[i] /= (*invPropSum)[i];
			}
		}

		delete g;
	}

	// Close the results file ...
	if (setups->isUseResultsFile()) resultsFile.close();

	// ...free allocated memory...
	delete invPropSum; delete Qt; delete Map;

	//... and return the result
	return res;
}

bool cossmosAlgo::getHasResults() const {
	return hasResults;
}

cossmosAlgo::cossmosAlgo(cossmosSetups* setups)
{
	this->setups = setups;
	hasResults = false;
	res = NULL;
}

cossmosAlgo::~cossmosAlgo()
{
	if (getHasResults()) delete res;
}
